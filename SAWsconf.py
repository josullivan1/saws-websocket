try:
  import argparse
except:
  import os
  print('Installation of Python does not have argparse. Run "sudo easy_intall argparse" then try configure again')
  os._exit(1)

def mkdir_p(path):
    import os
    try:
        os.makedirs(path)
    except OSError as exc:
        if exc.errno == os.errno.EEXIST:
            pass
        else: raise


# ConfigureAction from steven.bethard@gmail.com
# https://code.google.com/p/argparse/issues/detail?id=2

def strboolean(string):
    string = string.lower()
    if string in ['0', 'f', 'false', 'no', 'off']:
        return ''               # Empty string is False for Makefile
    elif string in ['1', 't', 'true', 'yes', 'on']:
        return 'True'
    else:
        raise ValueError()


class ConfigureAction(argparse.Action):
    def __init__(self,
                 option_strings,
                 dest,
                 default=None,
                 required=False,
                 help=None,
                 metavar=None,
                 positive_prefixes=['--', '--with-', '--enable-'],
                 negative_prefixes=['--no-', '--without-', '--disable-']):
        import re
        strings = []
        self.positive_strings = set()
        self.negative_strings = set()
        for string in option_strings:
            assert re.match(r'--[A-z]+', string)
            suffix = string[2:]
            for positive_prefix in positive_prefixes:
                self.positive_strings.add(positive_prefix + suffix)
                strings.append(positive_prefix + suffix)
            for negative_prefix in negative_prefixes:
                self.negative_strings.add(negative_prefix + suffix)
                strings.append(negative_prefix + suffix)
        super(ConfigureAction, self).__init__(
            option_strings=strings,
            dest=dest,
            nargs='?',
            const=None,
            default=default,
            type=strboolean,
            choices=None,
            required=required,
            help=help,
            metavar=metavar)

    def __call__(self, parser, namespace, value, option_string=None):
        if value is None:
            value = option_string in self.positive_strings
        elif option_string in self.negative_strings:
            value = not value
        setattr(namespace, self.dest, value)

def main(default=dict()):
    import sys,os

    # convert from ./configure type argument passing to ArgumentsParser style	
    args = []
    for i in sys.argv:
        if i.find('=') > -1 and not i[0] == '-': i = '--'+i
	args.extend([i])
    sys.argv = args	
	    
    parser = argparse.ArgumentParser(description='Configure Scientific Application Web server (SAWs)')
    parser.add_argument('--CC', help='Path to C compiler', default=default.get('CC','gcc'))
    parser.add_argument('--CFLAGS', help='Flags for C compiler', default=default.get('CFLAGS','-g3 -Wall -MMD -MP'))
    parser.add_argument('--LDFLAGS', help='Flags to pass to linker, e.g., -L<lib dir>', default=default.get('LDFLAGS',''))
    parser.add_argument('--LIBS', help='Extra libraries to link, e.g., -l<library>', default=default.get('LIBS','-lpthread -lm'))
    parser.add_argument('--prefix', help='Installation prefix', default=default.get('prefix','/usr/local'))
    parser.add_argument('--build-dir', help='Build directory', default=default.get('build_dir','build'))
    parser.add_argument('--enable-shared', help='Build shared libraries', default=default.get('enable_shared','True'), action=ConfigureAction)
    parser.add_argument('--CFLAGS-SHARED', help='CFLAGS needed when compiling for shared libraries', default=default.get('CFLAGS_SHARED','-fPIC'))

    #  These arguments are currently not used, need them here so parser will not complain about unknown options
    parser.add_argument('--CXX', help='Path to C++ compiler', default=None)
    parser.add_argument('--CXXFLAGS', help='C++ compiler flags', default=None)
    parser.add_argument('--MAKE', help='Path to make', default=None)
    parser.add_argument('--AR', help='Path to archive utility', default=None)
    parser.add_argument('--ARFLAGS', help='Flags for archive utility', default=None)
    parser.add_argument('--libdir', help='Location to put resulting library', default=None)
    parser.add_argument('--FC', help='Fortran compiler path', default=None)
    parser.add_argument('--FCFLAGS', help='Fortran compiler flags', default=None)
    parser.add_argument('--F90', help='Fortran compiler path', default=None)
    parser.add_argument('--F90FLAGS', help='Fortran compiler flags', default=None)
    parser.add_argument('--F77', help='Fortran compiler path', default=None)
    parser.add_argument('--FFLAGS', help='Fortran compiler flags', default=None)
    parser.add_argument('--disable-cxx', help='Do not use C++ compiler',action='store_true')
    parser.add_argument('--disable-fc', help='Do not use Fortran compiler', action='store_true')
    parser.add_argument('--disable-fortran', help='Do not use Fortran compiler', action='store_true')	
    parser.add_argument('--disable-f77', help='Do not use Fortran 77 compiler', action='store_true')
    parser.add_argument('--disable-f90', help='Do not use Fortran 90 compiler', action='store_true')
    args = parser.parse_args()
    args.build_dir = os.path.abspath(args.build_dir)
    args.SAWs_dir = os.path.dirname(os.path.abspath(__file__))
    mkdir_p(args.build_dir)
    configure(args)

def configure(args):
    import sys,os
    with open(os.path.join(args.build_dir, 'Makefile'), 'w') as f:
        f.write(
            'CC := %(CC)s\n'
            'CFLAGS := %(CFLAGS)s\n'
            'CFLAGS_SHARED := %(CFLAGS_SHARED)s\n'
            'LDFLAGS := %(LDFLAGS)s\n'
            'LIBS := %(LIBS)s\n'
            'SHARED := %(enable_shared)s\n'
            'prefix := %(prefix)s\n'
            'libdir := $(prefix)/lib\n'
            'includedir := $(prefix)/include\n'
            'build_dir := %(build_dir)s\n'
            'include %(SAWs_dir)s/base.mk\n'
            % args.__dict__)
    reconfname = os.path.join(args.build_dir,'reconfigure.py')
    with open(reconfname, 'w') as f:
        f.write(
            '#!%(python)s\n'
            'import os, sys\n'
            'ARGS = %(args)r\n'
            "sys.path.insert(0, os.path.abspath(ARGS['SAWs_dir']))\n"
            'import SAWsconf\n'
            'SAWsconf.main(ARGS)\n'
            % dict(python=sys.executable, args=args.__dict__))
    os.chmod(reconfname,0o755)
    # Attempt to write Makefile in source directory to help out people who don't read directions
    try:
        with open(os.path.join(args.SAWs_dir, 'Makefile'), 'w') as f:
            f.write(
                'all lib install clean etags:\n'
                '\t$(MAKE) -C %(build_dir)s $@\n'
                '\n'
                '.PHONY: all lib insall clean etags\n'
                % dict(build_dir=args.build_dir))
    except IOError:             # It is okay if source directory is not writeable
        pass
    print('Configuration complete in: %s' % os.path.relpath(args.build_dir))
    print('To build: make -j3 -C %s' % os.path.relpath(args.build_dir))
