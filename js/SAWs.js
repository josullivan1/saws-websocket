/*
 Copyright (c) 2013 UChicago Argonne, LLC

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

/*
   Provides a simple library of routines for getting and posting SAWs directories and values to the server and 
   displaying them in a browser
*/

/*
   Userful hints about jQuery and javascript syntax
   ------------------------------------------------

    jQuery.each(dictionary,function(key,value)) --- runs through the dictionary calling the function on each key,value pair
         - the function is called sequentially and synchronously once for each key, value pair
         - the function has access to the variables in the outer function (it is a "closure")
         - the outer function does not continue until all the key,value pairs are processed

    jQuery.getJSON(url,function(data)) ---- gets the data from the URL on the server and calls the function with that data 
         - the function is called asynchronously once the data arrives
         - the function has access to the variables in the outer function
         - the outer function continues possibly before the inner function is called or completed

    jQuery("#variablesInfo").  --- accesses the DOM object on the webpage name #variablesInto, i.e. <div id="variablesInfo" 

*/

/*name (or names), as referred to below, is a specific data structure that allows for fine grain
  choice of which directories (and variables) to get from the server. 
  names = {'dirName1':['varName1','varName2'],'dirName2':['varName1','varName2'],'dirName3':[],'dirName4:['varName4']}
  when the array [] is empty it requests all the variables
*/

/*
   SAWs functions are all collect in the SAWs namespace
*/

var url = 'ws://' + window.location.host + '/foo';
    
var websocket = new WebSocket(url);
websocket.onopen = function(ev) { websocket.send("open"); };
websocket.onclose = function(ev) { console.log("close"); };
websocket.onmessage = function(ev) {
  if (ev.data == "exit") {
    websocket.send('exit');
  }
};
websocket.onerror = function(ev) {
  console.log("error");
};

var SAWs = {}

SAWs.mapenabled = false;
SAWs.mapdata = "";
SAWs.markersArray = [];
SAWs.map;
SAWs.display;

SAWs.degrFrom_rad = function(rad) { return rad * 180 / Math.PI; }

SAWs.radFrom_degr  = function(degr) { return degr * Math.PI / 180; }

SAWs.createMarkerInMap = function(icon, lat, lng, message) {
    var location = new google.maps.LatLng(lat, lng);

    var marker = new google.maps.Marker({ position: location, map: SAWs.map, icon: icon });

    var content = '<div id="infoWindow"><h2>' + message + '</h2></div>';

    marker.info = new google.maps.InfoWindow({ content: content, disableAutoPan: true });

    google.maps.event.addListener(marker, 'mouseover', function() { marker.info.open(map, marker); });

    google.maps.event.addListener(marker, 'mouseout', function() { marker.info.close(); });

    google.maps.event.addListener(marker, 'click', function() { markerClicked(lat, lng, icon); });

    markersArray.push(marker);
}

SAWs.markerClicked = function(lat, lng, icon) { }

SAWs.createMarkerInMapArrow = function(lat, lng, angle) {
    var location = new google.maps.LatLng(lat, lng);

    var marker = new google.maps.Marker({ position: location, map: SAWs.map, icon: { path: google.maps.SymbolPath.FORWARD_OPEN_ARROW, scale: 4, rotation: angle } });

    var content = '<div id="infoWindow"></div>';

    marker.info = new google.maps.InfoWindow({ content: content, disableAutoPan: true });

    google.maps.event.addListener(marker, 'mouseover', function() { marker.info.open(map, marker); });

    google.maps.event.addListener(marker, 'mouseout', function() { marker.info.close(); });

    google.maps.event.addListener(marker, 'click', function() { markerClicked(lat, lng, icon); });

    SAWs.markersArray.push(marker);
  }

 SAWs.getAngle = function(startLat,startLong,endLat,endLong){
    startLat = SAWs.radFrom_degr(startLat), startLong = SAWs.radFrom_degr(startLong), endLat = SAWs.radFrom_degr(endLat), endLong = SAWs.radFrom_degr(endLong);

    var x = endLong - startLong;

    var a = Math.log(Math.tan(endLat/2.0+Math.PI/4.0)/Math.tan(startLat/2.0+Math.PI/4.0));

    if (Math.abs(x) > Math.PI){ if (x > 0.0) { x = -(2.0 * Math.PI - x); } else {  x = (2.0 * Math.PI + x); } }

    return (SAWs.degrFrom_rad(Math.atan2(x, a)) + 360.0) % 360.0;
  }

 SAWs.drawLine = function(long1,lat1,long2,lat2) {
    var lineone = [ new google.maps.LatLng(long1, lat1), new google.maps.LatLng(long2, lat2)];

    var line1 = new google.maps.Polyline({ path: lineone, geodesic: true, strokeColor: '#000000', strokeOpacity: 1.0, strokeWeight: 3 });

    line1.setMap(SAWs.map);
  }
  
 SAWs.findMidPoint = function(long1, lat1, long2, lat2) {
    var data = [[long1, lat1],[long2, lat2]], sumOFX=0, sumOFY=0, sumOFZ=0, newlat, newlng;

    for (var i = 0; i < data.length; i++) { newlat = SAWs.radFrom_degr(data[i][0]), newlng = SAWs.radFrom_degr(data[i][1]), sumOFX += Math.cos(newlat) * Math.cos(newlng), sumOFY += Math.cos(newlat) * Math.sin(newlng), sumOFZ += Math.sin(newlat); }

    var avgX = sumOFX / data.length, avgY = sumOFY / data.length, avgZ = sumOFZ / data.length;

    newlng = Math.atan2(avgY, avgX);

    var newhyp = Math.sqrt(avgX * avgX + avgY * avgY);

    newlat = Math.atan2(avgZ, newhyp);

    return [SAWs.degrFrom_rad(newlat), SAWs.degrFrom_rad(newlng)]

  }

SAWs.createArrowOnMidpoint = function(long1, lat1, long2, lat2) {
    var middat1 = SAWs.findMidPoint(long1, lat1,long2, lat2);

    SAWs.createMarkerInMapArrow(middat1[0],middat1[1],SAWs.getAngle(long1, lat1,long2, lat2));
  }
/*
  SAWs.getDirectory grabs the directories and variables listed in names  from the server

  For each entry in names (or once if names is null) the callback is called with the received information for that entry

*/
SAWs.getDirectory = function(names,callback,callbackdata) {
  /*If names is null, get all*/
  if(names == null){
    jQuery.getJSON('/SAWs/*',function(data){
                               if(typeof(callback) == typeof(Function)) callback(data,callbackdata)
                             })
  } else {
    jQuery.getJSON('/SAWs/' + names,function(data){
                                       if(typeof(callback) == typeof(Function)) callback(data,callbackdata)
                                    })

 //   jQuery.each(names,function(key,value){
   //                     var directory
     //                   directory = key + "/"
       //                 if(names[key].length == 0){
         //                  jQuery.getJSON('/SAWs/' + directory,function(data){
           //                                                      if(typeof(callback) == typeof(Function)) callback(data,callbackdata)
             //                                                  })
               //          } else {
                 //          for(var j = 0;j<names[key].length;j++){
                   //          jQuery.getJSON('/SAWs/' + directory + names[key][j],function(data){
                     //                                                              if(typeof(callback) == typeof(Function)) callback(data,callbackdata)
                       //                                                         })
                   //        }
                 //        }
                   //   })
  }
};


/*  
   SAWs.getAndDisplayDirectory(divEntry,names) - Gets the lastest values from the server and calls SAWs.displayDirectory() to display them
*/
SAWs.getAndDisplayDirectory = function(names,divEntry){
  jQuery(divEntry).html("")
  SAWs.getDirectory(names,SAWs.displayDirectory,divEntry)
}

/*
  SAWs.displayDirectory - displays the passed directory tree sub to the DOM specifically

  This saves the displayed directory in the ugly global variable called globaldirectory.
  This is so that if SAWs.updateAndPostDirectory(null) is called then that directory is used. 
  We should instead somehow save all downloaded directories (without duplicates) so that all 
  all of them could be updated and posted.

  if sub has a bool variable named __Block in the root directory then a "Continue" button is presented 
  that when pressed updates __Block to false posts an update of the directory to the server and 
  then gets a refresh of data from the server.

  If any entry is changed or receives a keyup (like a carriage return in a text box) then the attribute selected is added to the object which then
  gets added to the JSON sent back to the server

*/
var globaldirectory = {}

SAWs.displayDirectory = function(sub,divEntry)
{
  globaldirectory[divEntry] = sub
  if (sub.directories.SAWs_ROOT_DIRECTORY.variables.hasOwnProperty("__Block") && (sub.directories.SAWs_ROOT_DIRECTORY.variables.__Block.data[0] == "true")) {
    jQuery(divEntry).append("<center><input type=\"button\" value=\"Continue\" id=\"continue\"></center>")
    jQuery('#continue').on('click', function(){
                                      //websocket.send('exit');
                                      SAWs.updateDirectoryFromDisplay(divEntry)
                                      sub.directories.SAWs_ROOT_DIRECTORY.variables.__Block.data = ["false"];
                                      SAWs.postDirectory(sub);
                                      jQuery(divEntry).html("");
                                      window.setTimeout(SAWs.getAndDisplayDirectory,1000,null,divEntry);
                                    })
  }
  SAWs.displayDirectoryRecursive(sub.directories,divEntry,0,"")
}

SAWs.tab = function(key,tab)
{
  for (i=0;i<tab;i++){
    jQuery("#"+key).append("&nbsp;&nbsp;&nbsp;&nbsp;")
  }
}

SAWs.initMap = function() {

   
} 

SAWs.displayDirectoryRecursive = function(sub,divEntry,tab,fullkey)
{
  
  jQuery.each(sub,function(key,value){
                   fullkey = fullkey+key
                   if(jQuery("#"+fullkey).length == 0){
                     jQuery(divEntry).append("<div id =\""+fullkey+"\"></div>")
                     if (key != "SAWs_ROOT_DIRECTORY") {
			 SAWs.tab(fullkey,tab)
			 jQuery("#"+fullkey).append("<b>"+ key +"<b><br>")
                     }
                     jQuery.each(sub[key].variables, function(vKey, vValue) {
                                     if (vKey[0] != '_' || vKey[1] != '_' ) {
                                       SAWs.tab(fullkey,tab+1)
                                       if (vKey[0] != '_') {
                                         if(sub[key].variables[vKey].dtype == "SAWs_MAP") {
                                         } else {
                                         jQuery("#"+fullkey).append(vKey+":&nbsp;") }
                                       }
				       for(j=0;j<sub[key].variables[vKey].data.length;j++){
				        if (sub[key].variables[vKey].data[j].toString() != "(null)"){	     
                                         if(sub[key].variables[vKey].alternatives.length == 0){
                                           if(sub[key].variables[vKey].dtype == "SAWs_BOOLEAN") {
                                             jQuery("#"+fullkey).append("<select id=\"data"+fullkey+vKey+j+"\">")
                                             jQuery("#data"+fullkey+vKey+j).append("<option value=\"true\">True</option> <option value=\"false\">False</option>")
                                           } else {
                                             if(sub[key].variables[vKey].dtype == "SAWs_MAP") {
                                             } else {
                                             jQuery("#"+fullkey).append("<input type=\"text\" style=\"font-family: Courier\" size=\""+(sub[key].variables[vKey].data[j].toString().length+1)+"\" id=\"data"+fullkey+vKey+j+"\" name=\"data\" \\>")
                                             jQuery("#data"+fullkey+vKey+j).keyup(function(obj) {
                                                                                   console.log( "Key up called "+key+vKey );
                                                                                   sub[key].variables[vKey].selected = 1;
                                                                                  });
                                             }
                                           }

                                           if(sub[key].variables[vKey].dtype == "SAWs_MAP") {
                                              SAWs.mapenabled = true;
                                              SAWs.mapdata = SAWs.mapdata + sub[key].variables[vKey].data[0];
                                           } else {

                                           jQuery("#data"+fullkey+vKey+j).val(sub[key].variables[vKey].data[j])
                                           jQuery("#data"+fullkey+vKey+j).change(function(obj) {
                                                                                   console.log( "Change called"+key+vKey );
                                                                                   sub[key].variables[vKey].selected = 1;
                                                                                 });
                                           }
                                         } else {
                                           jQuery("#"+fullkey).append("<select id=\"data"+fullkey+vKey+j+"\">")
                                           jQuery("#data"+fullkey+vKey+j).append("<option value=\""+sub[key].variables[vKey].data[j]+"\">"+sub[key].variables[vKey].data[j]+"</option>")
                                           for(var l=0;l<sub[key].variables[vKey].alternatives.length;l++){
                                             jQuery("#data"+fullkey+vKey+j).append("<option value=\""+sub[key].variables[vKey].alternatives[l]+"\">"+sub[key].variables[vKey].alternatives[l]+"</option>")
                                           }
                                           jQuery("#"+fullkey).append("</select>")
                                           jQuery("#data"+fullkey+vKey+j).change(function(obj) {
                                                                                   console.log( "Change called"+key+vKey );
                                                                                   sub[key].variables[vKey].selected = 1;
                                                                                 });
                                         }
                                         if(sub[key].variables[vKey].mtype != "SAWs_WRITE") {
                                           jQuery("#data"+ fullkey+vKey+j).attr('readonly',true)
                                         } else {
                                           jQuery("#data"+ fullkey+vKey+j).attr('style',"color: #FF0000")
                                         }
					}    
                                       }
                                       jQuery("#"+fullkey).append("<br>")
                                     }                 
                                   })
                     if(typeof sub[key].directories != 'undefined'){
                       ++tab
                       SAWs.displayDirectoryRecursive(sub[key].directories,divEntry,tab,fullkey)
                       --tab
                     }
                   }
                })

    if (SAWs.mapenabled) {
      jQuery("head").append('<style>#graph-container { top: 0%; bottom: 0%; left: 50%; right: 0px; position: absolute; background: #ccc; } #container { position: relative; top: 0; left: 0; right: 0; height: 50%; background: #ccc; }</style>');
      jQuery(divEntry).before('<div id="container"><div id="graph-container"></div><div id="control-pane"><h2 class="underline">Details</h2></div><div id="map-canvas"></div></div><br><br>');  
      var xml = $(jQuery.parseXML(SAWs.mapdata));
      var g = { nodes: [], edges: [] };
      var mapOptions = { zoom: 4, center: new google.maps.LatLng(41.696760, -87.948319), mapTypeId: google.maps.MapTypeId.ROADMAP };
      SAWs.display = new google.maps.DirectionsRenderer();
      SAWs.map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
      SAWs.display.setMap(SAWs.map);                   
      var functarray = {};                
      $(xml).find('junction').each(function(){
        var id = $(this).find('id').text();
        var lat = $(this).find('lat').text();
        var lon = $(this).find('lon').text();
        var elev = $(this).find('elev').text();
        var offset = $(this).find('offset').text();
        functarray[id] = {"lat":lat,"lon":lon};
        g.nodes.push({ id: id, label: "ID: " + id + " Lat: " + lat + " Lon: " + lon + " Elev: " + elev + " Offset: " + offset, x: lat, y: -lon, size: 0, color: '#000000' });
      });

      $(xml).find('pipe').each(function(){
        var id = $(this).find('id').text();
        var from = $(this).find('numfrom').text();
        var to = $(this).find('numto').text();
        var elev = $(this).find('offset').text();
        var offset = $(this).find('nnodes').text();
        //SAWs.drawLine(functarray[from].lon, functarray[from].lat,functarray[to].lon, functarray[to].lat);
        g.edges.push({ id: 'e' + id, source: from, target: to, size: 1, color: '#fff'}); 
      });

      s = new sigma({ graph: g, container: 'graph-container' });

      s.bind('overNode outNode clickNode doubleClickNode rightClickNode', function(e) {
        console.log(e.type, e.data.node.label, e.data.captor);
        $("div#control-pane h2").html(e.data.node.label)
      });

    }
}


/*
   Sends a POST of the directory to the server to update variable values that are writable
*/
SAWs.postDirectory = function(directory){
  var stringJSON = JSON.stringify(directory)
  /* could you jQuery.post()? */
  jQuery.ajax({type: 'POST',dataType: 'json',url: '/SAWs/*',data: {input: stringJSON}})
}
      
/*
   Sends the data from the html page to the C program. 
*/    
SAWs.updateAndPostDirectory = function(divEntry) {
  SAWs.updateDirectoryFromDisplay(divEntry)
  SAWs.postDirectory(globaldirectory[divEntry])
};

/*
   Updates a directory object from any changes in the HTML GUI
*/
SAWs.updateDirectoryFromDisplay = function(divEntry) {
   sub = globaldirectory[divEntry]
  SAWs.updateDirectoryFromDisplayRecursive(sub.directories,"")
}  


/*
   Updates a directory object from any changes in the HTML GUI
*/
SAWs.updateDirectoryFromDisplayRecursive = function(sub,fullkey) {
   jQuery.each(sub,function(key,value){
                     fullkey = fullkey+key
                     jQuery.each(sub[key].variables,function(vKey,vValue){
                                                       for(var k = 0; k<sub[key].variables[vKey].data.length; k++){
                                                         if(jQuery("#data"+fullkey+vKey +k).length != 0) {
                                                           var data1 = jQuery("#data"+ fullkey+vKey+k).val();
                    
                                                           /*Parse the data approriately*/
                                                           if(sub[key].variables[vKey].dtype == "SAWs_INT"){     
                                                             if(!isNaN(parseInt(data1))){
                                                               sub[key].variables[vKey].data[k] = parseInt(data1)
                                                             }                   
                                                           } else if(sub[key].variables[vKey].dtype == "SAWs_DOUBLE" || sub[key].variables[vKey].dtype == "SAWs_FLOAT"){     
                                                             if(!isNaN(parseFloat(data1))){
                                                               sub[key].variables[vKey].data[k] = parseFloat(data1)
                                                             }
                                                           } else {
                                                             sub[key].variables[vKey].data[k] = data1
                                                           }
                                                         }
                                                       }
                                                     })
                     if(typeof sub[key].directories != 'undefined'){
                       SAWs.updateDirectoryFromDisplayRecursive(sub[key].directories,fullkey)
                     }
                   })
}
