# directory containing this file
BASEDIR := $(abspath $(dir $(lastword $(MAKEFILE_LIST))))

BINDIR := $(build_dir)/bin
LIBDIR := $(build_dir)/lib
SAWS_SRC.c := $(shell cd $(BASEDIR) && find src -name \*.c)
SAWS_SRC.o := $(SAWS_SRC.c:%.c=$(build_dir)/%.o)
EXE_SRC.c := $(shell cd $(BASEDIR) && find examples tests -name \*.c)
EXE_SRC.o := $(EXE_SRC.c:%.c=$(build_dir)/%.o)
ALL_SRC.o := $(SAWS_SRC.o) $(EXE_SRC.o)

vpath %.c $(BASEDIR)

libSAWs := $(LIBDIR)/$(if $(SHARED),libSAWs.so,libSAWs.a)

CFLAGS += -I$(BASEDIR)/include -I$(BASEDIR)/src -DNO_SSL -DNO_SSL_DLL -DUSE_WEBSOCKET
ifneq ($(SHARED),)
LDFLAGS_RPATH := -Wl,-rpath,$(dir $(libSAWs))
endif

.SECONDEXPANSION:		# to expand $$(@D)/.DIR

$(build_dir)/%.o: %.c | $$(@D)/.DIR
	$(CC) -c -o $@ $< $(CFLAGS) $(if $(SHARED),$(CFLAGS_SHARED))

all: lib Trivial SAWsInitHandler simpleJSONtoCStructTest SAWsInitHandlerUpdatingData simpleMultiDirectoryJSONTest simpleRegisterDeleteTest

simpleJSONtoCStructTest : $(BINDIR)/simpleJSONtoCStructTest
$(BINDIR)/simpleJSONtoCStructTest: $(build_dir)/tests/simpleJSONtoCStructTest.o $(libSAWs) | $$(@D)/.DIR
	$(CC) -o $@ $^ $(CFLAGS) $(LDFLAGS) $(LDFLAGS_RPATH) $(LIBS)

simpleRegisterDeleteTest : $(BINDIR)/simpleRegisterDeleteTest
$(BINDIR)/simpleRegisterDeleteTest: $(build_dir)/tests/simpleRegisterDeleteTest.o $(libSAWs) | $$(@D)/.DIR
	$(CC) -o $@ $^ $(CFLAGS) $(LDFLAGS) $(LDFLAGS_RPATH) $(LIBS)

simpleMultiDirectoryJSONTest : $(BINDIR)/simpleMultiDirectoryJSONTest
$(BINDIR)/simpleMultiDirectoryJSONTest: $(build_dir)/tests/simpleMultiDirectoryJSONTest.o $(libSAWs) | $$(@D)/.DIR
	$(CC) -o $@ $^ $(CFLAGS) $(LDFLAGS) $(LDFLAGS_RPATH) $(LIBS)

Trivial : $(BINDIR)/Trivial
$(BINDIR)/Trivial: $(build_dir)/examples/Trivial.o $(libSAWs) | $$(@D)/.DIR
	$(CC) -o $@ $(CFLAGS) $^ $(LDFLAGS) $(LDFLAGS_RPATH) $(LIBS)

SAWsInitHandler : $(BINDIR)/SAWsInitHandler
$(BINDIR)/SAWsInitHandler: $(build_dir)/examples/SAWsInitHandler.o $(libSAWs) | $$(@D)/.DIR
	$(CC) -o $@ $(CFLAGS) $^ $(LDFLAGS) $(LDFLAGS_RPATH) $(LIBS)

SAWsInitHandlerUpdatingData : $(BINDIR)/SAWsInitHandlerUpdatingData
$(BINDIR)/SAWsInitHandlerUpdatingData: $(build_dir)/examples/SAWsInitHandlerUpdatingData.o $(libSAWs) | $$(@D)/.DIR
	$(CC) -o $@ $(CFLAGS) $^ $(LDFLAGS) $(LDFLAGS_RPATH) $(LIBS)

lib: $(libSAWs) $(build_dir)/include/SAWs.h $(build_dir)/include/mongoose.h
$(LIBDIR)/libSAWs.a: $(SAWS_SRC.o) | $$(@D)/.DIR
	ar rc $@ $^
$(LIBDIR)/libSAWs.so: $(SAWS_SRC.o) | $$(@D)/.DIR
	$(CC) -shared -o $@ $^ $(LDFLAGS) $(LIBS)

$(build_dir)/include/SAWs.h : $(BASEDIR)/src/SAWs.h | $$(@D)/.DIR
	cp $^ $@

$(build_dir)/include/mongoose.h : $(BASEDIR)/src/mongoose.h | $$(@D)/.DIR
	cp $^ $@

install : lib
	install -d -m0755 "$(DESTDIR)$(libdir)"
	install -m0644 $(LIBDIR)/lib* "$(DESTDIR)$(libdir)"
	install -d -m0755 "$(DESTDIR)$(includedir)"
	install -m0644 $(build_dir)/include/*.h "$(DESTDIR)$(includedir)"

%/.DIR :
	@mkdir -p $(@D)
	@touch $@

.PRECIOUS: %/.DIR

.PHONY: all clean print libSAWs SAWsInitHandler install

clean:
	rm -rf $(build_dir)/src $(build_dir)/examples $(build_dir)/tests $(BINDIR) $(LIBDIR)

etags:
	etags ../src/*.c ../src/*.h ../doc/manual/SAWs.tex ../js/SAWs.js ../base.mk

# make print VAR=the-variable
print:
	@echo $($(VAR))

-include $(ALL_SRC.o:%.o=%.d)
