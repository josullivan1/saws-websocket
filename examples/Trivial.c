
#include <stdio.h>
#include <unistd.h>
#include "SAWs.h"

int main(int argc,char **args)
{
  double t = 3.14;
  char   fullurl[256];

  int portnumber;
  SAWs_Get_Available_Port(&portnumber);
  printf("Available port number %d\n",portnumber);

  SAWs_Set_Use_Logfile("SAWslogfile");
  SAWs_Initialize();
  SAWs_Get_FullURL(sizeof(fullurl),fullurl);
  printf("Point your browser to %s\n",fullurl);
  SAWs_Register("/Time",&t,1,SAWs_READ,SAWs_DOUBLE);
  sleep(100);
  SAWs_Finalize();
  return 0;
}   
