/*
 Copyright (c) 2013 UChicago Argonne, LLC

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#if !defined(__SAWs_h__)
#define __SAWs_h__
#if defined(__cplusplus)
extern "C" {
#endif

/*    
   SAWs_Get_Available_Port(int *portnumber);

   Gets a free port number that can be used with SAWs_Set_Port(). If you plan to run multiple instances of an
   application with SAWs you should use this routine to get a unique port for each instance.
*/
int SAWs_Get_Available_Port(int*);
/*   SAWs_Set_Use_HTTPS(const char* filenameofcertificate);

   Use HTTPS instead of HTTP for connections. Defaults to HTTP.   Must be called before SAWs_Initialize()
*/
int SAWs_Set_Use_HTTPS(const char*);
/*    
   SAWs_Set_Port(int portnumber);

   Port number to use for connections. Defaults to 8080.   Must be called before SAWs_Initialize()
*/
int SAWs_Set_Port(int);
/*
   SAWs_Set_Use_Logfile(const char *filename)

   Logs all requests to a file. If filename is NULL then default to SAWs.log.   Must be called before SAWs_Initialize()
*/
int SAWs_Set_Use_Logfile(const char*);
/*
   SAWs_Set_Document_Root(const char *directory)

  If set then the webserver will load any files requested that are in this directory. Defaults to not loading any files. Must be called before SAWs_Initialize().
  Use SAWs_Add_Document_Root() to provide additional directories to search.
*/
int SAWs_Set_Document_Root(const char*);
/*
   SAWs_Add_Document_Root(const char *directory)

  The webserver will load any files requested that are in this directory.  May be called multiple times and all directories provided will be 
  searched. Most be called after calling SAWs_Set_Document_Root()
*/
int SAWs_Add_Document_Root(const char*);
/*
  SAWs_Push_Header(const char *url,const char *htmltext) 

  Sets the header of the webpage to be displayed when the server is accessed with url and no file has has been provided in the document root directory with that name.

  Use SAWs_Pop_Header() to return to previous header

  Use htmltext of NULL to se default header

  May be called anytime, currently url must be "index.html"
*/
int SAWs_Push_Header(const char*,const char *);
/*
  SAWs_Pop_Header(const char *url)

  Returns header of the webpage to be displayed when the server is accessed with url back to the previous value

  Use SAWs_Push_Header() to set a new header

  May be called anytime, currently url must be "index.html"
*/
int SAWs_Pop_Header(const char*);
/*
  SAWs_Push_Local_Header(void);

  By default the SAWs HTML header will reference the SAWs Javascript files on the internet. If one is running the server and client locally without access
  to the internet the Javascript will not be accessible. This option forces the SAWs to look for the Javascript in the subdirectory js in the root 
  directory set with SAWs_Set_Document_Root(). You must copy the js directory from the SAWs directory to the root directory.

  Use SAWs_Pop_Header() to return to previous header

  May be called anytime, but should be called before SAWs_Initialize().
*/
int SAWs_Push_Local_Header(void);
/*
  SAWs_Push_Body(const char* url,int part,const char *htmltext) 

  Sets a portion of the body of the webpage to be displayed when the server is accessed with url and no file has has been provided in the document root directory with that name.
  The body has 3 parts accessible by 0, 1, and 2. If 0 or 2 is omitted and url is "index.html" then default portion of the body provided by SAWs will be used.
  If a body is provided for a URL but not for the header (with SAWs_Push_Header()) then the default header is used.

  Uses SAWs_Pop_Body() to return to using previously set body part. 

  Use htmltext = NULL to use the default bodypart

  May be called anytime  
*/
int SAWs_Push_Body(const char*,int,const char *);
/*
  SAWs_Pop_Body(const char* url,int part)

  Returns the body part to its previous value before the call to SAWs_Push_Body()

  Uses SAWs_Push_Body() to use a new body part

  May be called anytime  
*/
int SAWs_Pop_Body(const char*,int);
/*
   SAWs_Initialize(void)

   Starts up the webserver.  Must be called before any of the SAWs routines except thos beginning with SAWs_Set
*/
int SAWs_Initialize(void);
/*
   SAWs_WS_SendData(const char*)

   If the Websocket is connected users can send data to Web page.
*/
int SAWs_WS_SendData(char*);
/*
   SAWs_WS_Exit()

   Function to send the exit command to the websocket
*/
int SAWs_WS_Exit();
/*
   SAWs_WS_CallBack(void *)

   Function to set the websocket data processing callback of data sent from client
*/
int SAWs_WS_CallBack(void *);
/*
   SAWs_Finalize(void)

   Shuts down the webserver.   No SAWs calls may be made after this call.
*/
int SAWs_Finalize(void); 
/*    
   SAWs_Get_FullURL(size_t,char *);

   Gets the full URL for accessing the webserver that has been started with SAWs_Initialize().

   It may return for example:  http://barrysmachine.com:8080
*/
#include <string.h>  
int SAWs_Get_FullURL(size_t,char*);

typedef enum {SAWs_MEMORY_UNDEF,SAWs_READ,SAWs_WRITE} SAWs_Memory_type;
typedef enum {SAWs_DATA_UNDEF,SAWs_CHAR,SAWs_BOOLEAN,SAWs_INT,SAWs_FLOAT,SAWs_DOUBLE,SAWs_STRING,SAWs_MAP,SAWs_GPS} SAWs_Data_type;

typedef struct {
  int              code;
  char*            data;
} WebSocketRequest;

/*
   SAWs_Register(const char *variablepathandname,void* addr,int len,SAWs_Memory_type mtype,SAWs_Data_type dtype);

   Registers an array located at addr with size len with the webserver. This variable will now be available for remote access.

   variablepathandname is of the form [/dir1[/dir2[/dir3]]]/
   mtype determines if the client is allowed to change the variable (SAWs_WRITE) or only read it (SAWs_READ)
   dtype is one of SAWs_CHAR, SAWs_BOOLEAN, SAWs_INT, SAWs_FLOAT, SAWs_DOUBLE, or SAWs_STRING

   The caller is required to insure that addr remains valid until SAWs_Delete() or SAWs_Finalize() is called otherwise the program
   may crash if a client requests a no longer available addr.

   By convention variable names that begin with __ are not displayed in the Javascript GUIs provided with SAWs. Variables that 
   begin with _ are displayed in the GUI without the name attached to them; generally these would be read only labels

   Must be called after SAWs_Initialize()
*/
int SAWs_Register(const char *,void*,int,SAWs_Memory_type,SAWs_Data_type);
/*
   SAWs_Set_Legal_Variable_Values(char* varName,int numberOfValues,void *legalValues)

   Lists legal values for a previously registered variable

   Must be called after a corresponding SAWs_Register()
*/
int SAWs_Set_Legal_Variable_Values(const char*,int,void*);
/*
   SAWs_Delete(const char *variablepathandname)

   Removes a registered array. The variable will no longer be accessible by a client.  Must be called after a corresponding SAWs_Register()
*/
int SAWs_Delete(const char*);

/*
   SAWs_Lock(void)

   Temporarily stops clients from access values from the server. Should be followed soon by a SAWs_Unlock() or the client will not 
   be able to access the server.

   This should be called before any published variables values are changed to ensure consistent values are passed to the client. 

   Example:
      int variable[2];
      SAWs_Register("/myvariable",variable,2,SAWs_READ,SAWs_INT);
      ....
      SAWs_Lock();
      variable[0] 1; variable[1] = 2;
      SAWs_Unlock();

   Must be called after SAWs_Initialize()
*/
int SAWs_Lock(void);
/*
   SAWs_Unlock(void)

   Allows clients to access values from the server. Called after SAWs_Lock()

   Must be called after SAWs_Initialize()
*/
int SAWs_Unlock(void);
/*
   SAWs_Selected(const char *variablepathandname,int *flg)

   Returns if the variable has been selected by a client

   variablepathandname is of the form [/dir1[/dir2[/dir3]]]/

   Must be called after SAWs_Initialize() and after the variable name has been registered with SAWs_Register()
*/
int SAWs_Selected(const char *,int*);

/*
   SAWs error codes 
*/
#define SAWs_ERROR_NONE                  0
#define SAWs_ERROR_DIRECTORY_NOT_FOUND   1
#define SAWs_ERROR_VARIABLE_NOT_FOUND    2
#define SAWs_ERROR_NOT_NUMERIC           3
#define SAWs_ERROR_NOT_BOOLEAN           4
#define SAWs_ERROR_DATATYPE_NOT_FOUND    5
#define SAWs_ERROR_CJSON_ERROR           6
#define SAWs_ERROR_VARIABLE_LIST_NULL    7
#define SAWs_ERROR_NULL_DIRECTORY        8
#define SAWs_ERROR_DIRECTORY_EXISTS      9
#define SAWs_ERROR_VARIABLE_EXISTS       10
#define SAWs_ERROR_MONGOOSE_ERROR        11
#define SAWs_ERROR_NULL_VARIABLE         12
#define SAWs_ERROR_NULL_NAME             13
#define SAWs_ERROR_MEMORY                14
#define SAWs_ERROR_RANGE                 15
#define SAWs_ERROR_NO_SUPPORT            16
#define SAWs_ERROR_WRONG_STATE           17
#define SAWs_ERROR_LOG_FILE              18
#define SAWS_ERROR_SPACE                 19
#define SAWs_ERROR_SYSTEM_CALL           20

#if defined(__cplusplus)
}
#endif

#endif
