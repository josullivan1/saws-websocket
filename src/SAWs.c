/*
 Copyright (c) 2013 UChicago Argonne, LLC

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#include "SAWs.h"
#include "mongoose.h"
#include <pthread.h>


typedef struct _P_SAWs_Variable* SAWs_Variable;
typedef struct _P_SAWs_Directory* SAWs_Directory;
extern SAWs_Directory SAWs_ROOT_DIRECTORY;

struct _P_SAWs_Variable
{
  char             *name;
  void             *data;            /* raw data  */
  int              len;
  SAWs_Memory_type mtype;
  SAWs_Data_type   dtype;
  int              status;           /* 0 = unmodified, 1 = modified */
  SAWs_Variable    prev;             /* pointer to previous added variable */
  SAWs_Variable    next;             /* pointer to next added variable */
  void             *alternatives;
  int              altLen;
  int              selected;
};

struct _P_SAWs_Directory
 {
   char            *name;
   SAWs_Variable   variables;
   SAWs_Variable   tailVariable;
   int             nb_variables;
   SAWs_Directory  firstChild,nextSibling,parent;
   pthread_mutex_t dir_Lock;
};

typedef struct _p_SAWsText *SAWsText;
struct _p_SAWsText {
  SAWsText prev;
  char     *text;
};  

/* SAWs operations */
int SAWs_Add_Variable(SAWs_Directory,const char*,void*,int,SAWs_Memory_type,SAWs_Data_type);
int SAWs_Add_Directory(SAWs_Directory,const char*);
int SAWs_Destroy_Directory(SAWs_Directory*);
int SAWs_Lock_Directory(SAWs_Directory);
int SAWs_Unlock_Directory(SAWs_Directory);

 /*cstruct to JSON parser*/
int JSONParser_CStruct_to_JSON(char*,int);
int JSONParser_Single_CStruct_to_JSON(char*,SAWs_Directory,int);
int JSONParser_Single_Variable_CStruct_to_JSON(char*,SAWs_Directory,char*,int);
int JSONParser_Single_All_Variable_CStruct_to_JSON(char*,SAWs_Directory,int);

/*json to cStruct parser*/
int cJSON_to_CStruct_Parser(char*,char*,char*);

/*Directory search*/
int _P_SAWs_Directory_Search(SAWs_Directory*,char*);
int _P_SAWs_Create_Variable(SAWs_Variable*);
int _P_SAWs_Free_Variable_List(SAWs_Variable);
int _P_SAWs_Search_Directory_Pointer(SAWs_Directory currentDirectory,SAWs_Directory desiredDirectory);
int _P_SAWs_Directory_One_Level_Search(SAWs_Directory *dir,const char *dirName,int levels);
int _P_SAWs_Search_Variables(SAWs_Directory dir,const char *name);

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <mongoose.h>
#include <time.h>
#include <sys/stat.h>

/* The header for the http call */
static const char *ajax_reply_start =
  "HTTP/1.1 200 OK\r\n"
  "Cache: no-cache\r\n"
  "Content-Type: application/javascript\r\n"
  "\r\n";


/* Global static data structure so that it can be used by SAWs_Initialize and SAWs_Finalize "<script type=\"text/javascript\" src=\"http://www.mcs.anl.gov/research/projects/saws/js/SAWs.js\"></script>\n"\ */
static struct mg_context *ctx;

/*Statics for HTTPS SSL*/
static int  useHTTPS = 0;
static char SSLcert[50];

/*Statics for Port */
static char ports[10];
static int  defaultPort = 1;

static FILE *logFile;

/*Statics for document_root*/
static int  setDocumentRoot = 0;
static char documentRoot[1024];

#define DefaultHeader        "<head>\n"\
                             "<script type=\"text/javascript\" src=\"http://www.mcs.anl.gov/research/projects/saws/js/jquery-1.9.1.js\"></script>\n"\
                             "<script type=\"text/javascript\" src=\"SAWs.js\"></script>\n"\
                             "<script>\n"\
                             "jQuery(document).ready(function() {\n"\
                             "jQuery('#postData').on('click', function(){SAWs.updateAndPostDirectory(\"#variablesInfo\")})\n"\
                             "jQuery('#allDirectories').click(function(){SAWs.getAndDisplayDirectory(null,\"#variablesInfo\")})\n"\
                             "SAWs.getAndDisplayDirectory(null,\"#variablesInfo\")\n"\
                             "})\n"\
                             "</script>\n"\
                             "</head>\n"
/*
    Version used that looks for JS in the root directory, not at the bitbucket site. Note that this is mostly duplicative of above and 
    should be fixed.
*/
#define DefaultLocalHeader   "<head>\n"\
                             "<script type=\"text/javascript\" src=\"http://www.mcs.anl.gov/research/projects/saws/js/jquery-1.9.1.js\"></script>\n"\
                             "<script type=\"text/javascript\" src=\"SAWs.js\"></script>\n"\
                             "<link type=\"text/css\" href=\"styles.css\" rel=\"stylesheet\"/>"\
                             "<script type=\"text/javascript\" src=\"https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false\"></script>\n"\
                             "<script type=\"text/javascript\" src=\"sigma.min.js\"></script>\n"\
                             "<script type=\"text/javascript\" src=\"sigma.parsers.json.min.js\"></script>\n"\
                             "<link type=\"text/css\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css\" rel=\"stylesheet\"/>"\
                             "<script type=\"text/javascript\" src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js\"></script>\n"\
                             "<script>\n"\
                             "jQuery(document).ready(function() {\n"\
                             "jQuery('#postData').on('click', function(){SAWs.updateAndDirectory(\"#variablesInfo\")})\n"\
                             "jQuery('#allDirectories').click(function(){SAWs.getAndDisplayDirectory(null,\"#variablesInfo\")})\n"\
                             "SAWs.getAndDisplayDirectory(null,\"#variablesInfo\")\n"\
                             "google.maps.event.addDomListener(window, 'load', SAWs.initMap)"\
                             "})\n"\
                             "</script>\n"\
                             "</head>\n"
#define DefaultBodyTop     "<body>\n"\
                           "<center><h2> Scientific Application Web server </h2></center>\n"\
                           "The application you are attaching to has not provided an introduction page, see "\
                           "<a href=\"https://bitbucket.org/saws/saws\">https://bitbucket.org/saws/saws</a> "\
                           "for more information on SAWs or try appending /SAWs to the URL to see the published variables as raw data\n"
#define DefaultBodyBottom  "<center><input type=\"button\" class=\"btn btn-default\" value=\"Update all variables from server\" id=\"allDirectories\">\n"\
                           "<input type=\"button\" class=\"btn btn-default\" value=\"Update server with changes below\" id=\"postData\"></center>\n"\
                           "<br>\n"\
                           "<div id=\"variablesInfo\" style=\"float:left\"></div>\n"\
                           "<br>\n"\
                           "<br>\n"\
                           "<br>\n"\
                           "<br>\n"\
                           "<br>\n"\
                           "<br>\n"\
                           "</body>"
static struct _p_SAWsText baseheader = {NULL,DefaultLocalHeader},basebodytop = {NULL,DefaultBodyTop},basebodymiddle = {NULL,""} ,basebodybottom = {NULL,DefaultBodyBottom};
static SAWsText header = &baseheader;
static SAWsText body[3] = {&basebodytop,&basebodymiddle,&basebodybottom};

static int ajax_get_single_directory(struct mg_connection *conn,const struct mg_request_info *request_info,SAWs_Directory dir)
{
  int  json_Length  = 10000000;
  char *final_JSON  = malloc(json_Length);
  int  errorCode;
  
  /* parse the directory */
  errorCode = JSONParser_Single_CStruct_to_JSON(final_JSON,dir,json_Length);

  if (errorCode == SAWs_ERROR_DIRECTORY_NOT_FOUND) {
    mg_printf(conn,"%s",ajax_reply_start);
    mg_printf(conn,"directory not found");
  } else {
    mg_printf(conn, "%s", ajax_reply_start);
    mg_printf(conn, "%s", final_JSON);
  }
  free(final_JSON);
  return SAWs_ERROR_NONE;
}

static int ajax_get_single_variable(struct mg_connection *conn,const struct mg_request_info *request_info,SAWs_Directory dir,char *variableName)
{
  int  json_Length  = 10000000;
  char *final_JSON  = malloc(json_Length);
  int  errorCode;
  
  /* parse the directory */
  errorCode = JSONParser_Single_Variable_CStruct_to_JSON(final_JSON,dir,variableName,json_Length);

  if (errorCode == SAWs_ERROR_DIRECTORY_NOT_FOUND) {
    mg_printf(conn,"%s",ajax_reply_start);
    mg_printf(conn,"directory not found");
  } else if (errorCode == SAWs_ERROR_VARIABLE_NOT_FOUND) {
    mg_printf(conn,"%s",ajax_reply_start);
    mg_printf(conn,"variable not found");
  } else {
    mg_printf(conn, "%s", ajax_reply_start);
    mg_printf(conn, "%s", final_JSON);
  }
  free(final_JSON);
  return SAWs_ERROR_NONE;
}

static int ajax_get_single_directory_all_variables(struct mg_connection *conn,const struct mg_request_info *request_info,SAWs_Directory dir)
{
  int  json_Length  = 10000000;
  char *final_JSON  = malloc(json_Length);

  /* parse the variable */
  JSONParser_Single_All_Variable_CStruct_to_JSON(final_JSON,dir,json_Length);

  // Prepare the message we're going to send
  mg_printf(conn, "%s", ajax_reply_start);
  mg_printf(conn, "%s", final_JSON);
  free(final_JSON);
  return SAWs_ERROR_NONE;
}

static int ajax_get_all_directories(struct mg_connection *conn,const struct mg_request_info *request_info)
{
  int  json_Length  = 10000000;
  char *final_JSON = malloc(json_Length);

  JSONParser_CStruct_to_JSON(final_JSON,json_Length);
  mg_printf(conn, "%s", ajax_reply_start);
  mg_printf(conn, "%s", final_JSON);
  free(final_JSON);
  return SAWs_ERROR_NONE;
}

/* This handler alters the variable data from the input given in the JS page */
static int ajax_Change_Directory(struct mg_connection *conn,const struct mg_request_info *request_info,char *name)
{
  int  sizeOfDataArray = 10000000;
  char *totalData      = malloc(sizeOfDataArray),*inputData = malloc(sizeOfDataArray);
  char *inputDataPointer;
  int  len;

  /*mg_read reads all the data from the request*/
  len = mg_read(conn,totalData,sizeOfDataArray-1);

  /*mg_get_var searches the data received for only the "input" variable */
  mg_get_var(totalData,len,"input",inputData,sizeOfDataArray-1);

  inputDataPointer = strdup(inputData);

  cJSON_to_CStruct_Parser(inputDataPointer,name,NULL);
  free(inputDataPointer);
  free(totalData);
  free(inputData);
  return SAWs_ERROR_NONE;
}

/* This handler alters the variable data from the input given in the JS page */
static int ajax_Change_All_Directories(struct mg_connection *conn,const struct mg_request_info *request_info)
{
  int  sizeOfDataArray = 10000000;
  char *totalData      = malloc(sizeOfDataArray),*inputData = malloc(sizeOfDataArray);
  char *inputDataPointer;
  int  len;

  /*mg_read reads all the data from the request*/
  len = mg_read(conn, totalData, sizeOfDataArray-1);

  /*mg_get_var searches the data received for only the "input" variable */
  mg_get_var(totalData, len, "input", inputData, sizeOfDataArray-1);

  inputDataPointer = strdup(inputData);

  cJSON_to_CStruct_Parser(inputDataPointer,NULL,NULL);
  free(inputDataPointer);
  free(totalData);
  free(inputData);
  return SAWs_ERROR_NONE;
}

/*this is the main handler call. It switched based on what uri is in the request*/
static int begin_request_handler(struct mg_connection *conn)
{
  const struct mg_request_info *request_info = mg_get_request_info(conn);
  int                          processed = 1,dirFound = 0,variableFlag = 0;
  char                         *stringChunk,*directoryName,*variableName; 
  char                         *requestInfo = strdup(request_info->uri), *c_time_string;
  SAWs_Directory               dir = SAWs_ROOT_DIRECTORY,searchDir = SAWs_ROOT_DIRECTORY;

  if (logFile){
    time_t current_time;
    current_time  = time(NULL);
    c_time_string = ctime(&current_time);
  }

  /*This handler uses strtok to break it up by where the / is (like a directory)
   *And goes to the appropriate place based on the information from strtok
  */
  stringChunk = strtok(requestInfo, "/");
  
  if (stringChunk == NULL || !strcmp(stringChunk,"index.html")) {
    int found = 0;
    if (setDocumentRoot && stringChunk && !strcmp(stringChunk,"index.html")) {
      char        fname[1024];
      struct stat statbuf;
      snprintf(fname,1024,"%s/index.html",documentRoot);
      found = !stat(fname, &statbuf);
    }
    if (found) mg_send_file(conn,"index.html");
    else {
      char   *ibody;
      size_t len = 0;
      len += strlen(body[0]->text);
      len += strlen(body[2]->text);
      len += strlen(body[1]->text);
      ibody = (char*) malloc(len+3); if (!ibody) return SAWs_ERROR_MEMORY;
      snprintf(ibody,len+3,"%s%s%s",body[0]->text,body[1]->text,body[2]->text);
      mg_printf(conn,"HTTP/1.1 200 OK\r\n"
                     "Content-Type: text/html\r\n"
                     "Content-Length: %d\r\n"
                     "\r\n"
            	     "%s%s\r\n",(int)strlen(header->text)+(int)strlen(ibody),header->text,ibody);
      free(ibody);
    }
  } else if (strcmp(stringChunk,"SAWs") == 0) {
    stringChunk = strtok(NULL, "/");
    if (stringChunk == NULL || strcmp(stringChunk,"*") == 0) {
      /* /SAWs/ * */
      if (strcmp(request_info->request_method, "GET") == 0) {
        /*GET*/
        ajax_get_all_directories(conn,request_info);
        if (logFile){
          fprintf(logFile,"Time: %.*s, All Directories - GET, IP:Port: %ld:%d \n",(int)strlen(c_time_string)-1,c_time_string,request_info->remote_ip,request_info->remote_port);
          fflush(logFile);
        }
      } else if (strcmp(request_info->request_method, "POST") == 0) {
        /*POST*/
        ajax_Change_All_Directories(conn,request_info);
        if (logFile){
          fprintf(logFile,"Time: %.*s, All Directories - POST, IP:Port: %ld:%d \n",(int)strlen(c_time_string)-1,c_time_string,request_info->remote_ip,request_info->remote_port);
          fflush(logFile);
        }
      }
    } else {
      /* /SAWs/dirname */
      variableFlag = _P_SAWs_Search_Variables(SAWs_ROOT_DIRECTORY,stringChunk);
      if (variableFlag == SAWs_ERROR_VARIABLE_EXISTS){
        directoryName = "SAWs_ROOT_DIRECTORY";  
      } else {
        dir = dir->firstChild;
	searchDir = dir;
	while(dir != NULL){
	  directoryName = stringChunk;
	  while(dir != NULL){
	    dirFound = 0;
	    if (strcmp(dir->name,stringChunk) == 0){
	      dirFound = 1;
	      break;
	    }
	    dir = dir->nextSibling;
	    searchDir = dir;
	  }
	  stringChunk = strtok(NULL,"/");
	  if (dirFound == 0){
	    variableFlag = _P_SAWs_Search_Variables(SAWs_ROOT_DIRECTORY,directoryName);
	    if (variableFlag == SAWs_ERROR_VARIABLE_EXISTS){
	      stringChunk   = directoryName;
	      directoryName = "SAWs_ROOT_DIRECTORY";  
	      break;
	    }
	    mg_printf(conn,"%s",ajax_reply_start);
	    mg_printf(conn,"directory not found");
	    free(requestInfo);
	    return SAWs_ERROR_DIRECTORY_NOT_FOUND;
	  }

	  variableFlag = _P_SAWs_Search_Variables(searchDir,stringChunk);

	  if (stringChunk == NULL || variableFlag == SAWs_ERROR_VARIABLE_EXISTS || strcmp(stringChunk,"*") == 0) break;
	  dir = dir->firstChild;
	  searchDir = dir;
	}
      }
      if (stringChunk == NULL || strcmp(stringChunk,"*") == 0) {
        if (strcmp(request_info->request_method, "GET") == 0) {
          /*GET*/
          ajax_get_single_directory(conn,request_info,dir);
          if (logFile){
            fprintf(logFile,"Time: %.*s, Directory Name: %s - GET, IP:Port: %ld:%d\n",(int)strlen(c_time_string)-1,c_time_string,directoryName,request_info->remote_ip,request_info->remote_port);
            fflush(logFile);
          }
        } else if (strcmp(request_info->request_method, "POST") == 0) {
          /*POST*/
          ajax_Change_Directory(conn,request_info,directoryName);
          if (logFile){
            fprintf(logFile,"Time: %.*s, Directory Name: %s - POST, IP:Port: %ld:%d\n",(int)strlen(c_time_string)-1,c_time_string,directoryName,request_info->remote_ip,request_info->remote_port);
            fflush(logFile);
          }
        }
      } else if (variableFlag == SAWs_ERROR_VARIABLE_EXISTS){
        /* /SAWs/directory/memname/ */

        variableName = stringChunk;
        if (variableName == NULL || strcmp(variableName,"*") == 0){
          /* /SAWs/dirName/variable 
             or /SAWs/dirName/variable/ * */
          if (strcmp(request_info->request_method, "GET") == 0) {
            /*GET*/
            ajax_get_single_directory_all_variables(conn,request_info,dir);
            if (logFile){
              fprintf(logFile,"Time: %.*s, Directory Name: %s, All Variables - GET, IP:Port: %ld:%d \n",(int)strlen(c_time_string)-1,c_time_string,directoryName,request_info->remote_ip,request_info->remote_port);
              fflush(logFile);
            }
          } else if (strcmp(request_info->request_method, "POST") == 0) {
            /*POST*/
            if (logFile){
              fprintf(logFile,"Time: %.*s, Directory Name: %s, All Variables - POST, IP:Port: %ld:%d \n",(int)strlen(c_time_string)-1,c_time_string,directoryName,request_info->remote_ip,request_info->remote_port);
              fflush(logFile);
            }
          }          
        } else {
          /* /SAWs/directory/memname/variable/variablename */
          if (strcmp(request_info->request_method, "GET") == 0) {
            /*GET*/
            ajax_get_single_variable(conn,request_info,dir,variableName);
            if (logFile){
              fprintf(logFile,"Time: %.*s, Directory Name: %s, Variable Name: %s - GET IP:Port: %ld:%d \n",(int)strlen(c_time_string)-1,c_time_string,directoryName,variableName,request_info->remote_ip,request_info->remote_port);
              fflush(logFile);
            }
          } else if (strcmp(request_info->request_method, "POST") == 0) {
            /*POST*/
            if (logFile){
              fprintf(logFile,"Time: %.*s, Directory Name: %s, Variable Name: %s - POST IP:Port: %ld:%d \n",(int)strlen(c_time_string)-1,c_time_string,directoryName,variableName,request_info->remote_ip,request_info->remote_port);
              fflush(logFile);
            }
          }
        }
      }

    }
  } else {
    processed = 0;
    if (logFile){
      fprintf(logFile,"Time: %.*s, Did not handle; Passed to Mongoose. IP:Port: %ld:%d \n",(int)strlen(c_time_string)-1,c_time_string,request_info->remote_ip,request_info->remote_port);
      fflush(logFile);
    }
  }
  free(requestInfo);
  return processed;
}

/* Sets the function pointer for the websocket client callback */
void (*callback)();
int SAWs_WS_CallBack(void *pointer) {
  callback = pointer;
  return SAWs_ERROR_NONE;
}

/* Hold the current mg_conneciton for the weboscket support*/
static struct mg_connection *main; 

/* Once websocket is opened function will transmit data to all webosockets connected to current session */
int SAWs_WS_SendData(char *data) {
  mg_websocket_write(main, WEBSOCKET_OPCODE_TEXT, data, strlen(data));
  return SAWs_ERROR_NONE;
}

/* Must be called before SAWs_Finalize to close the websocket correctly. */
int SAWs_WS_Exit() {
  static const char *message = "exit";
  mg_websocket_write(main, WEBSOCKET_OPCODE_TEXT, message, strlen(message));
  return memcmp(message, "exit", 4);
}

/* websocket_ready_handler is trigged when weboskcet is conected to js */
static void websocket_ready_handler(struct mg_connection *conn) {
  static const char *message = "connected";
  main = conn;
  mg_websocket_write(conn, WEBSOCKET_OPCODE_TEXT, message, strlen(message));
}

/* websocket_data_handler is trigged when data sent from the js to the server */
static int websocket_data_handler(struct mg_connection *conn, int flags,
                                  char *data, size_t data_len) {
  (void) flags; 

  WebSocketRequest req;
  req.code = 2;
  req.data = data;

  callback(req);
  mg_websocket_write(conn, WEBSOCKET_OPCODE_TEXT, data, data_len);
  return memcmp(data, "exit", 4);
}

int SAWs_Initialize(void)
{
  int                 i = 0,portsInt,optionsLen = 9;
  const char          *options[optionsLen];
  struct mg_callbacks callbacks;
  SAWs_Variable       var;


  if (ctx) return SAWs_ERROR_WRONG_STATE;

  /*Don't change the order! Append new options at bottom, before NULL*/
  options[i] = "listening_ports";
  i++;
  if (defaultPort == 1){
    options[i] = "8080";
  } else {
    options[i] = ports;
  }
  portsInt=i;
  i++;
  options[i] = "num_threads";
  i++;
  options[i] = "5";
  i++;
  if (useHTTPS == 1){
    if (defaultPort == 1){
      options[portsInt] = "8080s";
    } else {
      strncat(ports,"s",1);
      options[portsInt] = ports;
    }
    options[i] = "ssl_certificate";
    i++;
    options[i] = SSLcert;
    i++;
  }
  if (setDocumentRoot == 1){
    options[i] = "document_root";
    i++;
    options[i] = documentRoot;
    i++;
  } else {
    options[i] = "document_root";
    i++;
    options[i] = "/noexistentdirectory";
    i++;
  }
  options[i] = NULL;

  /* Prepare callbacks structure. We have only one callback, the rest are NULL. */
  memset(&callbacks, 0, sizeof(callbacks));
  callbacks.begin_request = begin_request_handler;
  callbacks.websocket_ready = websocket_ready_handler;
  callbacks.websocket_data = websocket_data_handler;
  /*Start the web server.*/
  ctx = mg_start(&callbacks, NULL, options);
  if (ctx == NULL){
    if (logFile){
      char   *c_time_string;
      time_t current_time = time(NULL);
      c_time_string = ctime(&current_time);
      fprintf(logFile,"Time: %.*s Eror in Mongoose startup\n",(int)strlen(c_time_string)-1,c_time_string);
      fflush(logFile);
    }
    return SAWs_ERROR_MONGOOSE_ERROR;
  }
  SAWs_ROOT_DIRECTORY               = (SAWs_Directory) malloc(sizeof(struct _P_SAWs_Directory));
  SAWs_ROOT_DIRECTORY->name         = strdup("SAWs_ROOT_DIRECTORY");
  SAWs_ROOT_DIRECTORY->nb_variables = 0;
  _P_SAWs_Create_Variable(&var);
  SAWs_ROOT_DIRECTORY->variables    = var;
  SAWs_ROOT_DIRECTORY->tailVariable = var;
  SAWs_ROOT_DIRECTORY->parent       = NULL;
  SAWs_ROOT_DIRECTORY->firstChild   = NULL;
  SAWs_ROOT_DIRECTORY->nextSibling  = NULL;

  pthread_mutex_init(&(SAWs_ROOT_DIRECTORY->dir_Lock), NULL);
  if (logFile){
    char   *c_time_string;
    time_t current_time = time(NULL);
    c_time_string = ctime(&current_time);
    fprintf(logFile,"Time: %.*s Started up SAWs\n",(int)strlen(c_time_string)-1,c_time_string);
    fflush(logFile);
  }
  return SAWs_ERROR_NONE;
}

/*stop the server with the simple mg_stop() call. Maybe we will clean up threads here *later, as well.*/
int SAWs_Finalize(void)
{
  SAWs_Directory dir;
  if (ctx == NULL) return SAWs_ERROR_MONGOOSE_ERROR;
  mg_stop(ctx);

  dir=SAWs_ROOT_DIRECTORY->firstChild;
  while(SAWs_ROOT_DIRECTORY->firstChild != NULL){
    SAWs_Destroy_Directory(&dir);
    dir=SAWs_ROOT_DIRECTORY->firstChild;
  }
  
  SAWs_Lock();
  _P_SAWs_Free_Variable_List((SAWs_ROOT_DIRECTORY)->variables);
  
  /*unlock the directory, destroy the mutex*/
  SAWs_Unlock();
  pthread_mutex_destroy(&((SAWs_ROOT_DIRECTORY)->dir_Lock));

  free((SAWs_ROOT_DIRECTORY)->name);
  free(SAWs_ROOT_DIRECTORY);

  SAWs_ROOT_DIRECTORY = NULL;
  ctx                 = NULL;
  if (logFile){
    char   *c_time_string;
    time_t current_time = time(NULL);
    c_time_string = ctime(&current_time);
    fprintf(logFile,"Time: %.*s Shut down SAWs\n",(int)strlen(c_time_string)-1,c_time_string);
    fflush(logFile);
    fclose(logFile);
  }
  return SAWs_ERROR_NONE;
}

int SAWs_Set_Use_HTTPS(const char *certificateName)
{
  if (ctx) return SAWs_ERROR_WRONG_STATE;
  snprintf(SSLcert,50,"%s",certificateName);
  useHTTPS = 1;
  return SAWs_ERROR_NONE;
}

int SAWs_Set_Port(const int portNumber)
{
  if (ctx) return SAWs_ERROR_WRONG_STATE;
  snprintf(ports,10,"%d",portNumber);
  defaultPort = 0;
  return SAWs_ERROR_NONE;
}


#include <unistd.h>

int SAWs_Get_FullURL(size_t len,char* url)
{
  size_t llen;
  if (useHTTPS){
    strncpy(url,"https://",len);
    llen = 8;
  } else {
    strncpy(url,"http://",len);
    llen = 7;
  }
  if (llen >= len) return SAWS_ERROR_SPACE;
  gethostname(url+llen,len-llen);
  llen = strlen(url);
  if (llen >= len) return SAWS_ERROR_SPACE;  
  strncat(url+llen,":",len-llen);
  llen = strlen(url);
  if (llen+1 >= len) return SAWS_ERROR_SPACE;
  if (defaultPort) {
    strncat(url+llen,"8080",len-llen);
  } else {
    strncat(url+llen,ports,len-llen);
  }
  llen = strlen(url);
  if (llen+1 >= len) return SAWS_ERROR_SPACE;
  strncat(url+llen,"/",len-llen);
  return 0;
}

int SAWs_Set_Use_Logfile(const char *userLogFileName)
{
  if (ctx) return SAWs_ERROR_WRONG_STATE;
  if (userLogFileName == NULL){
    userLogFileName = "saws.log";
  }
  logFile = fopen(userLogFileName,"w");
  if (!logFile) return SAWs_ERROR_LOG_FILE;
  return SAWs_ERROR_NONE;
}

extern char* mg_document_roots[10];
extern int mg_number_document_roots;
int SAWs_Add_Document_Root(const char *userDocumentRoot)
{
  if (mg_number_document_roots > 10) return SAWS_ERROR_SPACE;
  mg_document_roots[mg_number_document_roots++] = strdup(userDocumentRoot);
  return SAWs_ERROR_NONE;
}

int SAWs_Set_Document_Root(const char *userDocumentRoot)
{
  if (ctx) return SAWs_ERROR_WRONG_STATE;
  setDocumentRoot = 1;
  snprintf(documentRoot,50,"%s",userDocumentRoot);
  return SAWs_ERROR_NONE;
}

/*
   Needs to be fixed to support multiple urls
*/
int SAWs_Push_Header(const char* url,const char *introduction)
{
  SAWsText h = header;
  /* need to add support for urls that are not index.html */
  if (strcmp(url,"index.html") != 0) return SAWs_ERROR_NO_SUPPORT;
  if (!introduction) introduction = DefaultLocalHeader;
  header = (SAWsText) malloc(sizeof(struct _p_SAWsText));
  if (!header) return SAWs_ERROR_MEMORY;
  header->text = strdup(introduction);
  if (!header->text) return SAWs_ERROR_MEMORY;
  header->prev = h;
  return SAWs_ERROR_NONE;
}

int SAWs_Pop_Header(const char* url)
{
  if (header->prev) {
    SAWsText h = header;
    header = header->prev;
    free(h->text);
    free(h);
  }
  return SAWs_ERROR_NONE;
}
  
int SAWs_Push_Local_Header(void)
{
  return SAWs_Push_Header("index.html",DefaultLocalHeader);
}

/*
   Needs to be fixed to support multiple urls
*/
int SAWs_Push_Body(const char* url,int part,const char *introduction)
{
  SAWsText h;
  /* need to add support for urls that are not index.html */
  if (strcmp(url,"index.html") != 0) return SAWs_ERROR_NO_SUPPORT;
  if (part < 0 || part > 2) return SAWs_ERROR_RANGE;
  if (!introduction) {
    if (part == 0) introduction = DefaultBodyTop;
    else if (part == 1) introduction = "";
    else introduction = DefaultBodyBottom;
  }
  h = body[part];
  body[part] = malloc(sizeof(struct _p_SAWsText));
  if (!body[part]) return SAWs_ERROR_MEMORY;
  body[part]->text = strdup(introduction);
  if (!body[part]->text) return SAWs_ERROR_MEMORY;
  body[part]->prev = h;
  return SAWs_ERROR_NONE;
}

int SAWs_Pop_Body(const char* url,int part)
{
  /* need to add support for urls that are not index.html */
  if (strcmp(url,"index.html") != 0) return SAWs_ERROR_NO_SUPPORT;
  if (part < 0 || part > 2) return SAWs_ERROR_RANGE;
  if (body[part]->prev) {
    SAWsText h = body[part];
    body[part] = body[part]->prev;
    free(h->text);
    free(h);
  }
  return SAWs_ERROR_NONE;
}

/*I used an extern to allow the request handler to find the directory
 *and this is the global call that goes with it.
 *
 *I was taugh global calls are evil, so there may be a better way
 *but it was not apparent to me*/
SAWs_Directory SAWs_ROOT_DIRECTORY;

static int _P_SAWs_Directory_Tree_Delete(SAWs_Directory currentDirectory,SAWs_Directory dir)
{
  SAWs_Directory previousDirectory = NULL;
 /*loop through all the siblings*/
  while(currentDirectory != NULL){
    /*If there pointers are the same, they are the same variable
     *I use this instead of name, in case a user decides to name
     *two variables the same thing */
    if (currentDirectory == dir){
      if (previousDirectory==NULL){
        /*we are at first child*/
        dir->parent->firstChild = dir->nextSibling;
      } else {
        previousDirectory->nextSibling = dir->nextSibling;
      }
      return SAWs_ERROR_NONE;
    }
    previousDirectory = currentDirectory;
    if (currentDirectory->firstChild != NULL){
      if (_P_SAWs_Directory_Tree_Delete(currentDirectory->firstChild,dir) == SAWs_ERROR_NONE){
        return SAWs_ERROR_NONE;
      }
    }
    currentDirectory  = currentDirectory->nextSibling;

  }

  return SAWs_ERROR_DIRECTORY_NOT_FOUND;
 
}

/* Allocates an empty variable for other functions to use
 */
int _P_SAWs_Create_Variable(SAWs_Variable *var)
{
  (*var)               = (SAWs_Variable) malloc(sizeof(struct _P_SAWs_Variable));
  /*add null variables*/
  (*var)->name         = NULL;
  (*var)->data         = NULL;
  (*var)->alternatives = NULL;
  (*var)->altLen       = 0;
  (*var)->len          = -1;
  (*var)->mtype        = SAWs_MEMORY_UNDEF;
  (*var)->dtype        = SAWs_DATA_UNDEF;
  (*var)->status       = 0;
  (*var)->selected     = 0;
  (*var)->prev         = NULL;
  (*var)->next         = NULL;
  return SAWs_ERROR_NONE;
}


/* This function frees an individual variable */
static int _P_SAWs_Free_Single_Variable(SAWs_Variable var)
{
  if (var == NULL) return SAWs_ERROR_NULL_VARIABLE;
  if (var->name) free(var->name);
  free(var);
  return SAWs_ERROR_NONE;
}

/* This function frees the whole list of variables */
int _P_SAWs_Free_Variable_List(SAWs_Variable var)
{
  SAWs_Variable individualVar = var;
  if (var == NULL) return SAWs_ERROR_VARIABLE_LIST_NULL;

  /*? Maybe I shouldn't use a while loop ?*/
  while(individualVar->next != NULL) {
    individualVar = individualVar->next;
    _P_SAWs_Free_Single_Variable(individualVar->prev);
  }
  _P_SAWs_Free_Single_Variable(individualVar);
  return SAWs_ERROR_NONE;
}


int _P_SAWs_Search_Variables(SAWs_Directory dir,const char *name)
{
  int           nb_variables,i;
  SAWs_Variable var;
  
  if (dir == NULL) return SAWs_ERROR_NULL_DIRECTORY;
  if (name == NULL) return SAWs_ERROR_NULL_NAME;
  nb_variables = dir->nb_variables;
  var = dir->variables;

  for(i=0;i<nb_variables;i++){
    if (strcmp(var->name,name) == 0){
      return SAWs_ERROR_VARIABLE_EXISTS;
    }
    var = var->next;
  }
  return SAWs_ERROR_NONE;
}

/* Adds a variable to the declared directory with the given parameters
 * SAWs_Directory dir - the directory the variable is to be added to
 * char *name - a string with the desired name of the added variables
 * void *data - the data to be added to the variable
 * int len - the length of that data array
 * SAWs_Memory_type mtype - the memory type (read/write)
 * SAWs_Data_type dtype - the data type (int, char, bool, etc)
 */
int SAWs_Add_Variable(SAWs_Directory dir,const char *name,void *data,int len,SAWs_Memory_type mtype,SAWs_Data_type dtype)
{
  SAWs_Variable var;

  if (dir == NULL) return SAWs_ERROR_NULL_DIRECTORY;
  if (_P_SAWs_Search_Variables(dir,name) == SAWs_ERROR_VARIABLE_EXISTS) return SAWs_ERROR_VARIABLE_EXISTS;
  var               = (SAWs_Variable) malloc(sizeof(struct _P_SAWs_Variable));
  var->name         = strdup(name);
  var->data         = data;
  var->alternatives = NULL;
  var->altLen       = 0;
  var->len          = len;
  var->mtype        = mtype;
  var->dtype        = dtype;
  var->status       = 0;
  var->selected     = 0;
  
  /*Lock the directory before any variable is added*/
  SAWs_Lock();

  /* The first variable */
  if (dir->variables == dir->tailVariable) {
    var->prev             = NULL;
    var->next             = dir->tailVariable;
    dir->variables          = var;
    dir->tailVariable->prev = var;
    /* The rest of the variables */ 
  } else {
    var->next             = dir->tailVariable;
    var->prev             = dir->tailVariable->prev;
    var->prev->next       = var;
    dir->tailVariable->prev = var;
  }
  dir->nb_variables = dir->nb_variables + 1;
  
  /*Unlock the directory*/
  SAWs_Unlock();
  return SAWs_ERROR_NONE;
}

/*Creates a directory. Accepts an argument of name and a directory.
 *To be useful, variables must be added to the directory
 */
int SAWs_Add_Directory(SAWs_Directory dirParent,const char *name)
{
  SAWs_Directory newDir;
  SAWs_Directory child = SAWs_ROOT_DIRECTORY;
  SAWs_Variable  var;

  if (dirParent == NULL) return SAWs_ERROR_NULL_DIRECTORY;
  newDir = (SAWs_Directory) malloc(sizeof(struct _P_SAWs_Directory));

    child = dirParent->firstChild;
  newDir->parent    = dirParent;
  if (child == NULL){
    dirParent->firstChild = newDir;
  }
  
  _P_SAWs_Create_Variable(&var);
  newDir->name           = strdup(name);
  newDir->nb_variables   = 0;
  newDir->variables      = var;
  newDir->tailVariable   = var;
  if (child == NULL){
    newDir->nextSibling  = NULL;
  } else {
    while(child->nextSibling != NULL){
      child = child->nextSibling;
    }
    child->nextSibling = newDir;
    newDir->nextSibling = NULL;
  }
  newDir->firstChild     = NULL;
  /*initialize the mutex */

  return SAWs_ERROR_NONE;
}

static int _P_SAWs_Destroy_Directory(SAWs_Directory currentDirectory)
{
  SAWs_Directory nextDirectory;

  /*loop through all the siblings*/
  while(currentDirectory != NULL){
    if (currentDirectory->firstChild != NULL){
      _P_SAWs_Destroy_Directory(currentDirectory->firstChild);
    }
    SAWs_Lock();
 
    if (currentDirectory->variables != NULL){
      _P_SAWs_Free_Variable_List(currentDirectory->variables);
    }
    /*unlock the directory, destroy the mutex*/
    SAWs_Unlock();

    free(currentDirectory->name);
    _P_SAWs_Directory_Tree_Delete(SAWs_ROOT_DIRECTORY,currentDirectory);

    nextDirectory = currentDirectory->nextSibling;

    free(currentDirectory);

    currentDirectory = NULL;
    currentDirectory = nextDirectory;
  }
  return SAWs_ERROR_NONE;
}

/*Frees the directory, including all of the directory's variables and all of the subdirectory's
 *call the private version so that all children derived from it are also free'd.
 */
int SAWs_Destroy_Directory(SAWs_Directory *dir)
{
  if ((*dir) == NULL) return SAWs_ERROR_NULL_DIRECTORY;
  if ((*dir)->firstChild != NULL){
    _P_SAWs_Destroy_Directory(((*dir)->firstChild));
  }

  SAWs_Lock();
  _P_SAWs_Free_Variable_List((*dir)->variables);
  SAWs_Unlock();

  free((*dir)->name);
  /*Remove the dir from the tree, unless it is the root*/
  _P_SAWs_Directory_Tree_Delete(SAWs_ROOT_DIRECTORY,*dir);
  free(*dir);
  
  *dir = NULL;
  return SAWs_ERROR_NONE;
}


/*Using a simple mutex, this function locks the directory.
 *should be called anytime a directory's data is being changed
 */
int SAWs_Lock_Directory(SAWs_Directory dir)
{
  if (dir == NULL) return SAWs_ERROR_NULL_DIRECTORY;
  pthread_mutex_lock(&(dir->dir_Lock));
  return SAWs_ERROR_NONE;
}

/*Unlocks a simple mutex. Should be called after SAWs_Lock_Directory,
 *once the calling thread has finished changing the data
 */
int SAWs_Unlock_Directory(SAWs_Directory dir)
{
  if (dir == NULL) return SAWs_ERROR_NULL_DIRECTORY;
  pthread_mutex_unlock(&(dir->dir_Lock));
  return SAWs_ERROR_NONE;
}

int _P_SAWs_Directory_One_Level_Search(SAWs_Directory *dir,const char *dirName,int levels)
{
  int i;

  for(i=0;i<levels;i++){
    if (!(*dir)) return SAWs_ERROR_DIRECTORY_NOT_FOUND;
    (*dir) = (*dir)->firstChild;
  }
  while((*dir) != NULL){
    if (strcmp((*dir)->name,dirName) == 0) return SAWs_ERROR_NONE;
    (*dir) = (*dir)->nextSibling;
  }
  return SAWs_ERROR_DIRECTORY_NOT_FOUND;
}

int SAWs_Register(const char *userDirLocation,void *data,int len,SAWs_Memory_type mtype,SAWs_Data_type dtype)
{
  char           *stringChunk,*directoryName,*previousStringChunk,*dirLocation;
  SAWs_Directory parentDir = SAWs_ROOT_DIRECTORY,searchDir;

  dirLocation         = strdup(userDirLocation);
  stringChunk         = strtok(dirLocation,"/");
  previousStringChunk = stringChunk;
  stringChunk         = strtok(NULL,"/");

  /* /variableName */
  if (stringChunk == NULL){
    SAWs_Add_Variable(SAWs_ROOT_DIRECTORY,previousStringChunk,data,len,mtype,dtype);
  } else {
    /*/dirName/...../variableName*/
    /*Search the tree to see if dirName exists; if it doesn't add it it*/
    searchDir     = SAWs_ROOT_DIRECTORY;
    while(stringChunk != NULL){

      directoryName = previousStringChunk;
      if (_P_SAWs_Directory_One_Level_Search(&searchDir,directoryName,1) == SAWs_ERROR_DIRECTORY_NOT_FOUND){
        SAWs_Add_Directory(parentDir,directoryName);
      }
      previousStringChunk = stringChunk;
      stringChunk = strtok(NULL,"/");
      _P_SAWs_Directory_One_Level_Search(&parentDir,directoryName,1);
      searchDir = parentDir;
    }
    SAWs_Add_Variable(parentDir,previousStringChunk,data,len,mtype,dtype);
  }
  free(dirLocation);
  return SAWs_ERROR_NONE;
}

int _P_SAWs_Remove_Variable(SAWs_Directory *dir,const char *name)
{
  int           nb_variables,i;
  SAWs_Variable var;
  
  nb_variables = (*dir)->nb_variables;
  var          = (*dir)->variables;

  for(i=0;i<nb_variables;i++){
    if (strcmp(var->name,name) == 0){
      break;
    }
    var = var->next;
  }

  if (var->prev != NULL){
    var->prev->next = var->next;
  } else {
    (*dir)->variables = var->next;
  }

  if (var->next == (*dir)->tailVariable){
    (*dir)->tailVariable->prev = var->prev;
  } else {
    var->next->prev = var->prev;
  }
  
  i = 0;
  if (var->next == (*dir)->tailVariable && var->prev == NULL){
    i = 1;
  }

  _P_SAWs_Free_Single_Variable(var);

  if (i==1){
    (*dir)->variables=(*dir)->tailVariable;
  }

  (*dir)->nb_variables--;
  return SAWs_ERROR_NONE;
}

int _P_SAWs_Find_Variable(SAWs_Directory dir,const char *name,SAWs_Variable *outvar)
{
  int           nb_variables,i;
  SAWs_Variable var;
  
  nb_variables = (dir)->nb_variables;
  var          = (dir)->variables;

  for(i=0;i<nb_variables;i++){
    if (strcmp(var->name,name) == 0){
      break;
    }
    var = var->next;
  }
  *outvar = var;
  return SAWs_ERROR_NONE;
}

/*Adds a list of legal values to a variable. Accepts the directory where the variable is located,
 *the variable name, the number of legal values, and a void* list of legal values */
int SAWs_Set_Legal_Variable_Values(const char* varName,int numberOfValues,void *legalValues)
{
  char           *stringChunk,*directoryName,*previousStringChunk,*dirLocation;
  SAWs_Directory saveDir,searchDir = SAWs_ROOT_DIRECTORY;
  int            err = 0;

  dirLocation         = strdup(varName);
  stringChunk         = strtok(dirLocation,"/");
  previousStringChunk = stringChunk;
  stringChunk         = strtok(NULL,"/");

  /*dirName/...../variableName*/
  /*Seach the tree to see if dirName exists*/
  searchDir     = SAWs_ROOT_DIRECTORY;
  
  while(stringChunk != NULL){
    directoryName       = previousStringChunk;
    previousStringChunk = stringChunk;
    stringChunk         = strtok(NULL,"/");
    if (_P_SAWs_Directory_One_Level_Search(&searchDir,directoryName,1) == SAWs_ERROR_NONE){
      if (stringChunk == NULL){
	saveDir = searchDir;
	if (_P_SAWs_Search_Variables(saveDir,previousStringChunk) == SAWs_ERROR_VARIABLE_EXISTS){
          SAWs_Variable var;
          err = _P_SAWs_Find_Variable(saveDir,previousStringChunk,&var);if (err) return err;

          var->altLen       = numberOfValues;
          var->alternatives = legalValues;
	  free(dirLocation);
	  return SAWs_ERROR_NONE;
	}
      }
    } else {
      break;
    }
  }
  free(dirLocation);
  return SAWs_ERROR_VARIABLE_NOT_FOUND;
}

int SAWs_Delete(const char *userDirLocation)
{
  char           *stringChunk,*directoryName,*previousStringChunk,*dirLocation;
  SAWs_Directory saveDir,searchDir = SAWs_ROOT_DIRECTORY;

  dirLocation         = strdup(userDirLocation);
  stringChunk         = strtok(dirLocation,"/");
  previousStringChunk = stringChunk;
  stringChunk         = strtok(NULL,"/");

  if (stringChunk == NULL){
    if (_P_SAWs_Search_Variables(SAWs_ROOT_DIRECTORY,previousStringChunk) == SAWs_ERROR_VARIABLE_EXISTS){
      _P_SAWs_Remove_Variable(&SAWs_ROOT_DIRECTORY,previousStringChunk);
      free(dirLocation);
      return SAWs_ERROR_NONE;
    } else if (_P_SAWs_Directory_One_Level_Search(&searchDir,previousStringChunk,1) == SAWs_ERROR_NONE){
      SAWs_Destroy_Directory(&searchDir);
      free(dirLocation);
      return SAWs_ERROR_NONE;
    }
  } else {
    /*dirName/...../variableName*/
    /*Seach the tree to see if dirName exists*/
    searchDir     = SAWs_ROOT_DIRECTORY;

    while(stringChunk != NULL){
      directoryName       = previousStringChunk;
      previousStringChunk = stringChunk;
      stringChunk         = strtok(NULL,"/");
      if (_P_SAWs_Directory_One_Level_Search(&searchDir,directoryName,1) == SAWs_ERROR_NONE){
        if (stringChunk == NULL){
          saveDir = searchDir;
          if (_P_SAWs_Directory_One_Level_Search(&searchDir,previousStringChunk,1) == SAWs_ERROR_NONE){
            SAWs_Destroy_Directory(&searchDir);
            free(dirLocation);
            return SAWs_ERROR_NONE;
          } else if (_P_SAWs_Search_Variables(saveDir,previousStringChunk) == SAWs_ERROR_VARIABLE_EXISTS){
            _P_SAWs_Remove_Variable(&saveDir,previousStringChunk);
            free(dirLocation);
            return SAWs_ERROR_NONE;
          }
        }
      } else {
        break;
      }
    }
  }
  free(dirLocation);
  return SAWs_ERROR_DIRECTORY_NOT_FOUND;
}

int SAWs_Selected(const char *userDirLocation,int *flg)
{
  char           *stringChunk,*directoryName,*previousStringChunk,*dirLocation;
  SAWs_Directory saveDir,searchDir = SAWs_ROOT_DIRECTORY;
  int            err = 0;

  *flg = 0;
  dirLocation         = strdup(userDirLocation);
  stringChunk         = strtok(dirLocation,"/");
  previousStringChunk = stringChunk;
  stringChunk         = strtok(NULL,"/");

  /*dirName/...../variableName*/
  /*Seach the tree to see if dirName exists*/
  searchDir     = SAWs_ROOT_DIRECTORY;
  
  while(stringChunk != NULL){
    directoryName       = previousStringChunk;
    previousStringChunk = stringChunk;
    stringChunk         = strtok(NULL,"/");
    if (_P_SAWs_Directory_One_Level_Search(&searchDir,directoryName,1) == SAWs_ERROR_NONE){
      if (stringChunk == NULL){
	saveDir = searchDir;
	if (_P_SAWs_Search_Variables(saveDir,previousStringChunk) == SAWs_ERROR_VARIABLE_EXISTS){
          SAWs_Variable var;
          err = _P_SAWs_Find_Variable(saveDir,previousStringChunk,&var);if (err) return err;
          *flg = var->selected;
          var->selected = 0;
	  free(dirLocation);
	  return SAWs_ERROR_NONE;
	}
      }
    } else {
      break;
    }
  }
  free(dirLocation);
  return SAWs_ERROR_VARIABLE_NOT_FOUND;
}

int SAWs_Lock()
{
  pthread_mutex_lock(&(SAWs_ROOT_DIRECTORY->dir_Lock));
  return SAWs_ERROR_NONE;
}

int SAWs_Unlock()
{
  pthread_mutex_unlock(&(SAWs_ROOT_DIRECTORY->dir_Lock));
  return SAWs_ERROR_NONE;
}

/* This simple function checks the SAWs_Data_type and returns a string based on its value *
* None of the other petsc code returns anything but error codes, so maybe I shouldn't return strings */
static int dtype_Enum_to_String(SAWs_Data_type dtype,char** dtype_String)
{
  if (dtype == SAWs_DATA_UNDEF) {
    *dtype_String = "SAWs_DATA_UNDEF";
    return SAWs_ERROR_NONE;
  }

  if (dtype == SAWs_CHAR) {
    *dtype_String =  "SAWs_CHAR";
    return SAWs_ERROR_NONE;
  }

  if (dtype == SAWs_BOOLEAN) {
    *dtype_String = "SAWs_BOOLEAN";
    return SAWs_ERROR_NONE;
  }

  if (dtype == SAWs_FLOAT) {
    *dtype_String = "SAWs_FLOAT";
    return SAWs_ERROR_NONE;
  }

  if (dtype == SAWs_DOUBLE) {
    *dtype_String = "SAWs_DOUBLE";
    return SAWs_ERROR_NONE;
  }

  if (dtype == SAWs_STRING) {
    *dtype_String = "SAWs_STRING";
    return SAWs_ERROR_NONE;
  }

  if (dtype == SAWs_MAP) {
    *dtype_String = "SAWs_MAP";
    return SAWs_ERROR_NONE;
  }

  if (dtype == SAWs_GPS) {
    *dtype_String = "SAWs_GPS";
    return SAWs_ERROR_NONE;
  }

  if (dtype == SAWs_INT) {
    *dtype_String = "SAWs_INT";
    return SAWs_ERROR_NONE;
  }
  return SAWs_ERROR_NONE;
}


/* This simple function checks the SAWs_Memory_type and returns a string based on its value *
 * None of the other petsc code returns anything but error codes, so maybe I shouldn't return strings */
static int mtype_Enum_to_String(SAWs_Memory_type mtype,char** mtype_String)
{
  if (mtype == SAWs_MEMORY_UNDEF) {
    *mtype_String = "SAWs_MEMORY_UNDEF";
    return SAWs_ERROR_NONE;
  }

  if (mtype == SAWs_READ) {
    *mtype_String = "SAWs_READ";
    return SAWs_ERROR_NONE;
  }

  if (mtype == SAWs_WRITE) {
   *mtype_String = "SAWs_WRITE";
   return SAWs_ERROR_NONE;
  }
  return SAWs_ERROR_NONE;
}

/* This function turns the data into a string, using SAWs_Data_type to decide what case to use */
static int data_to_String(SAWs_Data_type dtype,void *data,char *data_String,int length,int data_String_Remaining)
{
  int   i;
  char  *value_String = malloc(102400);

  snprintf(data_String,data_String_Remaining,"\"data\": [");
  data_String_Remaining -= 13;

  /*Loop over the length of the array, printing each to a variable of the data string*/
  for(i=0;i<length;i++){
    /*? How should we handle this ?*/
    if (dtype == SAWs_DATA_UNDEF) {
      //return "SAWs_DATA_UNDEF";
    }

    if (dtype == SAWs_CHAR) {
      snprintf(value_String,1024,"\"%c\"",((char*)data)[i]);
    }
    
    if (dtype == SAWs_BOOLEAN) {
      snprintf(value_String,1024,"\"%s\"",((int*)data)[i] ? "true" : "false");
    }

    if (dtype == SAWs_FLOAT) {
      snprintf(value_String,1024,"%f",((float*)data)[i]);
    }

    if (dtype == SAWs_DOUBLE) {
      snprintf(value_String,1024,"%lf",((double*)data)[i]);
    }

    if (dtype == SAWs_STRING) {
      snprintf(value_String,1024,"\"%s\"",((char**)data)[i]);
    }

    if (dtype == SAWs_MAP) {
      snprintf(value_String,1024,"\"%s\"",((char**)data)[i]);
    }

    if (dtype == SAWs_GPS) {
      snprintf(value_String,1024,"\"%s\"",((char**)data)[i]);
    }

    if (dtype == SAWs_INT) {
      snprintf(value_String,1024,"%d",((int*)data)[i]);
    }
    strncat(data_String,value_String,data_String_Remaining);
    data_String_Remaining -= strlen(value_String);

    /*Add a comma (for JSON format) on all but the last*/
    if ((i+1) != length){
      strncat(data_String,",",data_String_Remaining);
      data_String_Remaining -= 1;
    }
  }

  strncat(data_String,"]",data_String_Remaining);
  data_String_Remaining -=1;
  free(value_String);
  return SAWs_ERROR_NONE;
}

int _P_SAWs_Search_Directory_Pointer(SAWs_Directory currentDirectory,SAWs_Directory desiredDirectory)
{
  int            found_Directory_Flag = SAWs_ERROR_DIRECTORY_NOT_FOUND;
  SAWs_Directory saveDirectory;
  if (currentDirectory == desiredDirectory) return SAWs_ERROR_NONE;

  /*loop through all the siblings*/
  while(currentDirectory != NULL){
    if (currentDirectory == desiredDirectory){
      return SAWs_ERROR_NONE;
    }
    if (currentDirectory->firstChild != NULL){
      if (found_Directory_Flag == SAWs_ERROR_DIRECTORY_NOT_FOUND){
        saveDirectory = currentDirectory;
        currentDirectory = currentDirectory->firstChild;
        found_Directory_Flag = _P_SAWs_Search_Directory_Pointer(currentDirectory,desiredDirectory);
        if (found_Directory_Flag == SAWs_ERROR_DIRECTORY_NOT_FOUND){
          currentDirectory = saveDirectory;
        }
      } else if (found_Directory_Flag == SAWs_ERROR_NONE){
        return SAWs_ERROR_NONE;
      }
    }
    if (found_Directory_Flag == SAWs_ERROR_DIRECTORY_NOT_FOUND){
      currentDirectory  = currentDirectory->nextSibling;
    } else if (found_Directory_Flag == SAWs_ERROR_NONE){
      return SAWs_ERROR_NONE;
    }
  }
  return SAWs_ERROR_DIRECTORY_NOT_FOUND;
}

int _P_SAWs_Directory_Search(SAWs_Directory *currentDirectory,char *dirName)
{
  int            found_Directory_Flag = SAWs_ERROR_DIRECTORY_NOT_FOUND;
  SAWs_Directory saveDirectory;

  if (strcmp((*currentDirectory)->name,dirName) == 0) return SAWs_ERROR_NONE;

  /*loop through all the siblings*/
  while((*currentDirectory) != NULL){
    if (strcmp((*currentDirectory)->name,dirName) == 0) return SAWs_ERROR_NONE;
    if ((*currentDirectory)->firstChild != NULL){
      if (found_Directory_Flag == SAWs_ERROR_DIRECTORY_NOT_FOUND){
        saveDirectory = (*currentDirectory);
        (*currentDirectory) = (*currentDirectory)->firstChild;
        found_Directory_Flag = _P_SAWs_Directory_Search(&(*currentDirectory),dirName);
        if (found_Directory_Flag == SAWs_ERROR_DIRECTORY_NOT_FOUND){
          (*currentDirectory) = saveDirectory;
        }
      } else if (found_Directory_Flag == SAWs_ERROR_NONE){
        return SAWs_ERROR_NONE;
      }
    }
    if (found_Directory_Flag == SAWs_ERROR_DIRECTORY_NOT_FOUND){
      (*currentDirectory)  = (*currentDirectory)->nextSibling;
    } else if (found_Directory_Flag == SAWs_ERROR_NONE){
      return SAWs_ERROR_NONE;
    }
  }
  return SAWs_ERROR_DIRECTORY_NOT_FOUND;
}

/* This function turns the data into a string, using SAWs_Data_type to decide what case to use */
static int alternatives_to_String(SAWs_Data_type dtype,void *data,char *data_String,int length,int alternatives_String_Remaining)
{
  int   i;
  char  *value_String = malloc(102400);

  snprintf(data_String,alternatives_String_Remaining,"\"alternatives\": [");
  alternatives_String_Remaining -=21;

  /*Loop over the length of the array, printing each to a variable of the data string*/
  for(i=0;i<length;i++){
    /*? How should we handle this ?*/
    if (dtype == SAWs_DATA_UNDEF) {
      //return "SAWs_DATA_UNDEF";
    }

    if (dtype == SAWs_CHAR) {
      snprintf(value_String,1024,"\"%c\"",((char*)data)[i]);
    }
    
    if (dtype == SAWs_BOOLEAN) {
      snprintf(value_String,1024,"\"%s\"",((int*)data)[i] ? "true" : "false");
    }

    if (dtype == SAWs_FLOAT) {
      /*?How many characters would a float be?*/
      snprintf(value_String,1024,"%f",((float*)data)[i]);
    }

    if (dtype == SAWs_DOUBLE) {
      snprintf(value_String,1024,"%lf",((double*)data)[i]);
    }

    if (dtype == SAWs_STRING) {
      snprintf(value_String,1024,"\"%s\"",((char**)data)[i]);
    }

    if (dtype == SAWs_MAP) {
      snprintf(value_String,1024,"\"%s\"",((char**)data)[i]);
    }

    if (dtype == SAWs_GPS) {
      snprintf(value_String,1024,"\"%s\"",((char**)data)[i]);
    }

    if (dtype == SAWs_INT) {
      snprintf(value_String,1024,"%d",((int*)data)[i]);
    }

    strncat(data_String,value_String,alternatives_String_Remaining);
    alternatives_String_Remaining -= strlen(value_String);

    /*Add a comma (for JSON format) on all but the last*/
    if ((i+1) != length){
      strncat(data_String,",",alternatives_String_Remaining);
      alternatives_String_Remaining -= 1;
    }
  }

  strncat(data_String,"]",alternatives_String_Remaining);
  alternatives_String_Remaining -= 1;
  free(value_String);
  return SAWs_ERROR_NONE;
}

/* This function takes a specific variable and converts it into a JSON Serialized String 
 * if all variables are to be grabbed, allFlag should be set to 1.
 */
static int _P_JSONParser_Single_Variable_CStruct_to_JSON(char *final_JSON_Serialized_Object,SAWs_Directory dir,char* variableName,int *json_Remaining)
{
  /* Declare variables to convert each individual object to a string (which will cocatenated) */
  /*! find a better way to dynamically allocate memory !*/
  char           *name_String,*data_String,len_String[30],dtype_String[50];
  char           *dtype, *mtype,*alternatives_String;
  char           mtype_String[50];
  SAWs_Variable  fld;
  int            lengthForJSONObject;
  int            data_String_Length = 102400,alternatives_String_Length = 1024000,found_Variable_Flag = 0;
  char           *JSON_Serialized_Object;

  data_String    = malloc(data_String_Length);
  alternatives_String = malloc(alternatives_String_Length);

  fld = dir->variables;
  
  /*Find the named variable*/
  while(fld->next != NULL){
    if (strcmp(fld->name,variableName) == 0){
      found_Variable_Flag = 1;
      break;
    }
    fld = fld->next;
  }
  
  if (found_Variable_Flag != 1) {
    free(data_String);
    free(alternatives_String);
    return SAWs_ERROR_VARIABLE_NOT_FOUND;
  }

  /* Allocate the right amount of memory */
  /*! find a better way to dynamically allocate memory !*/
  name_String    = malloc(17 + strlen(fld->name));

    /* Convert the individual variable info into individual strings */
  dtype_Enum_to_String(fld->dtype,&dtype);
  snprintf(name_String,(17+strlen(fld->name)),"\"%s\":",fld->name);
  snprintf(dtype_String,50,"\"dtype\": \"%s\"",dtype);
  mtype_Enum_to_String(fld->mtype,&mtype);
  snprintf(mtype_String,50,"\"mtype\": \"%s\"",mtype);
  data_to_String(fld->dtype,fld->data,data_String,fld->len,data_String_Length);
  snprintf(len_String,30,"\"length\": \"%d\"",fld->len);
  if (fld->alternatives != NULL){
    alternatives_to_String(fld->dtype,fld->alternatives,alternatives_String,fld->altLen,alternatives_String_Length);
  } else {
    snprintf(alternatives_String,alternatives_String_Length,"\"alternatives\": []");
  }
  
  lengthForJSONObject = strlen(name_String) + strlen(data_String) + strlen(dtype_String) + strlen(len_String) + strlen(len_String) + strlen(mtype_String) + strlen(alternatives_String) + 43;
  JSON_Serialized_Object = malloc(lengthForJSONObject);
  /* turn the individual variable into one long string */
  snprintf(JSON_Serialized_Object,lengthForJSONObject,"%s { \n %s,\n %s, \n %s, \n %s, \n %s}\n",name_String,data_String,dtype_String,len_String,mtype_String,alternatives_String);
 
  /* write each variable to the final string */
  strncat(final_JSON_Serialized_Object,JSON_Serialized_Object,*json_Remaining);
  *json_Remaining -= strlen(JSON_Serialized_Object);

  /* free the memory */
  free(name_String);
  free(data_String);
  free(JSON_Serialized_Object);
  free(alternatives_String);
  return SAWs_ERROR_NONE;
}

/* This function takes a directory and converts it, including all of its variables, into a JSON Serialized String 
 *if we are being called from the function to do all directories, allFlag should be set to 1*/

static int _P_JSONParser_Single_CStruct_to_JSON(char *final_JSON_Serialized_Object,SAWs_Directory dir,int siblings,int *json_Remaining)
{
  /* Declare variables to convert each individual object to a string (which will cocatenated) */
  /*! find a better way to dynamically allocate memory !*/
  int            nb_variables,i=0,j=0;
  char           *name_String;
  SAWs_Variable  fld;
  SAWs_Directory currentDirectory = dir;

  /*loop through all the siblings*/
  while(currentDirectory != NULL){
    if (currentDirectory->firstChild != NULL){
       if (j != 0){
         strncat(final_JSON_Serialized_Object,"},",*json_Remaining);
         *json_Remaining -= 2;
      } 
      
      nb_variables = currentDirectory->nb_variables;
      name_String = malloc(21 + strlen(currentDirectory->name));

      snprintf(name_String,(21 + strlen(currentDirectory->name)),"\"%s\":\n{",currentDirectory->name);
    
      strncat(final_JSON_Serialized_Object,name_String,*json_Remaining);
      *json_Remaining -= strlen(name_String);
      /* write the first part of the string */
      strncat(final_JSON_Serialized_Object,"\n \"variables\": {\n",*json_Remaining);
      *json_Remaining -= 16;
      fld = currentDirectory->variables;
    
      /* Loop over variables to obtain all the data from each name */
      for(i=0; i<nb_variables; i++) {
        _P_JSONParser_Single_Variable_CStruct_to_JSON(final_JSON_Serialized_Object,currentDirectory,fld->name,&(*json_Remaining));
        fld = fld->next;
        if (i + 1 != nb_variables){
          strncat(final_JSON_Serialized_Object,",",*json_Remaining);
          *json_Remaining -= 1;
        }
      }

      /* write the final part of the string */
      strncat(final_JSON_Serialized_Object,"}\n",*json_Remaining);
      *json_Remaining -= 2;
      if (currentDirectory != NULL){
        strncat(final_JSON_Serialized_Object,",",*json_Remaining);
        *json_Remaining -= 1;
      }
      strncat(final_JSON_Serialized_Object,"\"directories\":{",*json_Remaining);
      *json_Remaining -= 15;
      _P_JSONParser_Single_CStruct_to_JSON(final_JSON_Serialized_Object,currentDirectory->firstChild,1,&(*json_Remaining));
      strncat(final_JSON_Serialized_Object,"}",*json_Remaining);
      *json_Remaining -= 2;
      if (siblings != 0){
        currentDirectory = currentDirectory->nextSibling;
      } else {
        currentDirectory = NULL;
      }
      strncat(final_JSON_Serialized_Object,"}\n",*json_Remaining);
    } else {
      if (j != 0){
        strncat(final_JSON_Serialized_Object,"},",*json_Remaining);
        *json_Remaining -= 2;
      } 
      nb_variables = currentDirectory->nb_variables;
      name_String = malloc(21 + strlen(currentDirectory->name));
      snprintf(name_String,(21 + strlen(currentDirectory->name)),"\"%s\":\n{",currentDirectory->name);
    
      strncat(final_JSON_Serialized_Object,name_String,*json_Remaining);
      *json_Remaining -= strlen(name_String);
      /* write the first part of the string */
      strncat(final_JSON_Serialized_Object,"\n \"variables\": {\n",*json_Remaining);
      *json_Remaining -= 16;
      fld = currentDirectory->variables;
    
      /* Loop over variables to obtain all the data from each name */
      for(i=0; i<nb_variables; i++) {
        _P_JSONParser_Single_Variable_CStruct_to_JSON(final_JSON_Serialized_Object,currentDirectory,fld->name,&(*json_Remaining));
        fld = fld->next;
        if (i + 1 != nb_variables){
          strncat(final_JSON_Serialized_Object,",",*json_Remaining);
          *json_Remaining -= 1;
        }
      }

      /* write the final part of the string */
      strncat(final_JSON_Serialized_Object,"}\n",*json_Remaining);
      *json_Remaining -= 2;
      if (siblings != 0){
        currentDirectory = currentDirectory->nextSibling;
      } else {
        currentDirectory = NULL;
      }
    }
    j++;
    free(name_String);
  }
  return SAWs_ERROR_NONE;
}

/* This function takes all directories converts it, including all of its variables, into a JSON Serialized String */

int JSONParser_CStruct_to_JSON(char *final_JSON_Serialized_Object,int json_Remaining)
{
  snprintf(final_JSON_Serialized_Object,json_Remaining,"{\"directories\": {\n");
  json_Remaining -= 22;

  _P_JSONParser_Single_CStruct_to_JSON(final_JSON_Serialized_Object,SAWs_ROOT_DIRECTORY,1,&json_Remaining);
  /* write the final part of the string */
  strncat(final_JSON_Serialized_Object,"}}\n",json_Remaining);
  json_Remaining -= 3;
  strncat(final_JSON_Serialized_Object, "}\n",json_Remaining);
  json_Remaining -= 2;
  return SAWs_ERROR_NONE;
}

/* This function takes a directory and converts it, including all of its variables, but not its subdirectories, into a JSON Serialized String 
 */
int JSONParser_Single_All_Variable_CStruct_to_JSON(char *final_JSON_Serialized_Object,SAWs_Directory dir,int json_Remaining)
{
  SAWs_Variable  fld;
  int            i,nb_variables;
  char           *name_String;

  if (!dir) dir = SAWs_ROOT_DIRECTORY;  
  snprintf(final_JSON_Serialized_Object,json_Remaining,"{\"directories\": {\n");
  json_Remaining -= 22;

  nb_variables = dir->nb_variables;
  name_String = malloc(21 + strlen(dir->name));

  snprintf(name_String,(21 + strlen(dir->name)),"\"%s\":\n{",dir->name);
    
  strncat(final_JSON_Serialized_Object,name_String,json_Remaining);
  json_Remaining -= strlen(name_String);
  /* write the first part of the string */
  strncat(final_JSON_Serialized_Object,"\n \"variables\": {\n",json_Remaining);
  json_Remaining -= 16;
  fld = dir->variables;
      
  /* Loop over variables to obtain all the data from each name */
  for(i=0; i<nb_variables; i++) {
        
    _P_JSONParser_Single_Variable_CStruct_to_JSON(final_JSON_Serialized_Object,dir,fld->name,&(json_Remaining));
        
    fld = fld->next;
    if (i + 1 != nb_variables){
      strncat(final_JSON_Serialized_Object,",",json_Remaining);
      json_Remaining -= 1;
    }
  }

  /* write the final part of the string */
  strncat(final_JSON_Serialized_Object,"}}\n",json_Remaining);
  json_Remaining -= 2;
  
  /* write the final part of the string */
  strncat(final_JSON_Serialized_Object,"}}\n",json_Remaining);
  json_Remaining -= 3;
  strncat(final_JSON_Serialized_Object, "}\n",json_Remaining);
  json_Remaining -= 2;
  free(name_String);
  return SAWs_ERROR_NONE;
}

/* Takes a directory and converts it, including all of its variables, subdirectories, and all of their variables into a JSON Serialized String 
*/
int JSONParser_Single_CStruct_to_JSON(char *final_JSON_Serialized_Object,SAWs_Directory dir,int json_Remaining)
{
  snprintf(final_JSON_Serialized_Object,json_Remaining,"{\"directories\": {\n");
  json_Remaining -= 22;

  _P_JSONParser_Single_CStruct_to_JSON(final_JSON_Serialized_Object,dir,0,&json_Remaining);

  /* write the final part of the string */
  strncat(final_JSON_Serialized_Object,"}}\n",json_Remaining);
  json_Remaining -= 3;
  strncat(final_JSON_Serialized_Object, "}\n",json_Remaining);
  json_Remaining -= 2;
  return SAWs_ERROR_NONE;
}

int JSONParser_Single_Variable_CStruct_to_JSON(char *final_JSON_Serialized_Object,SAWs_Directory dir,char* variableName,int json_Remaining)
{

  char* nameString;

  if (!dir) dir = SAWs_ROOT_DIRECTORY;
  nameString = malloc(22+strlen(dir->name));
  
  snprintf(final_JSON_Serialized_Object,json_Remaining,"{\"directories\": {\n");
  json_Remaining -= 22;
  snprintf(nameString,(22+strlen(dir->name)),"\"%s\":\n{",dir->name);
  strncat(final_JSON_Serialized_Object,nameString,json_Remaining);
  json_Remaining -= strlen(nameString);
  strncat(final_JSON_Serialized_Object,"\n \"variables\": {\n",json_Remaining);
  json_Remaining -= 16;

  _P_JSONParser_Single_Variable_CStruct_to_JSON(final_JSON_Serialized_Object,dir,variableName,&json_Remaining);

  /* write the final part of the string */
  strncat(final_JSON_Serialized_Object,"}\n}",json_Remaining);
  json_Remaining -= 3;
  strncat(final_JSON_Serialized_Object,"}\n",json_Remaining);
  json_Remaining -= 2;
  strncat(final_JSON_Serialized_Object,"}\n",json_Remaining);
  json_Remaining -= 2;
  free(nameString);
  return SAWs_ERROR_NONE;
}

#include <cJSON.h>

static int _P_cJSON_to_CStruct_Data(cJSON *specificVariable,SAWs_Variable fld)
{
  cJSON *jsonData;
  char  *boolString;
  int   j,k,data_Length;

  if (fld->mtype == SAWs_WRITE){
    jsonData = cJSON_GetObjectItem(specificVariable,"selected");
    if (jsonData) {
      fld->selected = 1;
    } else {
      fld->selected = 0;
    }

    jsonData = cJSON_GetObjectItem(specificVariable,"data");
    data_Length = cJSON_GetArraySize(jsonData);
    
    for(j=0;j<data_Length;j++){
    
      /*Switch on data type*/
      if (fld->dtype == SAWs_STRING){
        /* this is wrong, we cannot free space allocated by the user */
        free(((char**)fld->data)[j]);
        ((char**)fld->data)[j] = strdup((char*)cJSON_GetArrayItem(jsonData,j)->valuestring);
      }

      if (fld->dtype == SAWs_MAP){
        /* this is wrong, we cannot free space allocated by the user */
        free(((char**)fld->data)[j]);
        ((char**)fld->data)[j] = strdup((char*)cJSON_GetArrayItem(jsonData,j)->valuestring);
      }

      if (fld->dtype == SAWs_GPS){
        /* this is wrong, we cannot free space allocated by the user */
        free(((char**)fld->data)[j]);
        ((char**)fld->data)[j] = strdup((char*)cJSON_GetArrayItem(jsonData,j)->valuestring);
      }
      
      if (fld->dtype == SAWs_CHAR){
        ((char*)fld->data)[j] = (char)cJSON_GetArrayItem(jsonData,j)->valuestring[0];
      }
      
      if (fld->dtype == SAWs_DOUBLE){
        ((double*)fld->data)[j] = (double)cJSON_GetArrayItem(jsonData,j)->valuedouble;
      }        
      
      if (fld->dtype == SAWs_FLOAT){
        ((float*)fld->data)[j] = (float)cJSON_GetArrayItem(jsonData,j)->valuedouble;
      }
      
      if (fld->dtype == SAWs_INT){
        ((int*)fld->data)[j] = (int)cJSON_GetArrayItem(jsonData,j)->valueint;
      }
      
      if (fld->dtype == SAWs_BOOLEAN){
        boolString = (char*)cJSON_GetArrayItem(jsonData,j)->valuestring;
        
        /*convert to lower case*/
        for(k=0;k<strlen(boolString);k++){
          boolString[k] = tolower(boolString[k]);
        }
        
        if (strcmp(boolString,"true") == 0) {
          ((int*)fld->data)[j]   = 1;
        } else if (strcmp(boolString,"false") == 0) {
          ((int*)fld->data)[j]   = 0;
        }
      }
    }
  }
  return SAWs_ERROR_NONE;
}

static int _P_cJSON_to_CStruct_Variable(cJSON *specificDirectory,SAWs_Directory amem,char *variableName)
{
  char           *currentName;
  cJSON          *variables,*specificVariable;
  SAWs_Variable  fld;

  variables = cJSON_GetObjectItem(specificDirectory,"variables");
  /*find the directory and variable in SAWs_ROOT_DIRECTORY*/
  fld = amem->variables;
  while(fld->next != NULL){
    specificVariable = cJSON_GetObjectItem(variables,fld->name);
    currentName      = fld->name;  
    
    /*if variableName != NULL, only changed the specified variable. IF variableName is NULL, do all variables*/
    if (specificVariable != NULL){
      if (variableName != NULL){
        if (strcmp(currentName,variableName) == 0){
          _P_cJSON_to_CStruct_Data(specificVariable,fld);
        }
      } else {
        _P_cJSON_to_CStruct_Data(specificVariable,fld);
      }
    }
    fld = fld->next;
  }
  return SAWs_ERROR_NONE;
}

static int _P_cJSON_to_CStruct_Recursion(cJSON *directories,char *directoryName,char *variableName,SAWs_Directory dir)
{  
  cJSON          *subDirectories,*specificDirectory;
  char           *currentName;
  
  /*Find the named directory, loop through the directories*/
  while(dir != NULL){
    specificDirectory = cJSON_GetObjectItem(directories,dir->name);
    subDirectories    = cJSON_GetObjectItem(specificDirectory,"directories");
    if (subDirectories != NULL){
      _P_cJSON_to_CStruct_Recursion(subDirectories,directoryName,variableName,dir->firstChild);
      currentName = dir->name;
    }
    if (directoryName != NULL){
      if (strcmp(currentName,directoryName) == 0){        
        _P_cJSON_to_CStruct_Variable(specificDirectory,dir,variableName);
      }
    } else {
      _P_cJSON_to_CStruct_Variable(specificDirectory,dir,variableName);
    }
    dir = dir->nextSibling;
  }
  return SAWs_ERROR_NONE;
}

int cJSON_to_CStruct_Parser(char *JSON_Serialized_Object,char *directoryName,char *variableName)
{
  cJSON *json,*directories;

  /*Parse the whole JSON*/
  json = cJSON_Parse(JSON_Serialized_Object);
  if (!json) {
    if (logFile){
      char   *c_time_string;
      time_t current_time = time(NULL);
      c_time_string = ctime(&current_time);
      fprintf(logFile,"Time: %.*s Error parsing %s in cJSON_to_CStruct_Parser() %s\n",(int)strlen(c_time_string)-1,c_time_string,JSON_Serialized_Object,cJSON_GetErrorPtr());
      fflush(logFile);
    }
    return SAWs_ERROR_CJSON_ERROR;
  }

  /*Get just the directories and number of directories*/
  directories = cJSON_GetObjectItem(json,"directories");
  if (!directories) {
    if (logFile){
      char   *c_time_string;
      time_t current_time = time(NULL);
      c_time_string = ctime(&current_time);
      fprintf(logFile,"Time: %.*s Error accessing directories in %s in cJSON_GetObjectItem() %s\n",(int)strlen(c_time_string)-1,c_time_string,JSON_Serialized_Object,cJSON_GetErrorPtr());
      fflush(logFile);
    }
    return SAWs_ERROR_CJSON_ERROR;
  }
  
  _P_cJSON_to_CStruct_Recursion(directories,directoryName,variableName,SAWs_ROOT_DIRECTORY);

  cJSON_Delete(json);
  return SAWs_ERROR_NONE;
}

#include <sys/socket.h>
#include <netinet/in.h>

int SAWs_Get_Available_Port(int *portNumber)
{
  struct sockaddr_in  saddr;
  socklen_t           slen = sizeof(saddr);
  
  *portNumber = 8080;
  int s = socket(AF_INET,SOCK_STREAM,0);if (s == -1) return SAWs_ERROR_SYSTEM_CALL;
    //  s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
  memset(&saddr,0,sizeof(saddr));
  saddr.sin_port   = htons(0);
  if (bind(s,(struct sockaddr*)&saddr,sizeof(saddr)) < 0) return SAWs_ERROR_SYSTEM_CALL;
  //addr = s.getsockname()
  //print addr[1]
  if (getsockname(s, (struct sockaddr*)&saddr, &slen) < 0) return SAWs_ERROR_SYSTEM_CALL;
  *portNumber = saddr.sin_port;
  close(s);  
  return SAWs_ERROR_NONE;
}
