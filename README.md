
static char help[] = "This is example of how to use the SAWs library with websocket";

#include "../src/wash.h"
#include <petscdmnetwork.h>
#include <petscviewersaws.h>
#include <pthread.h>

#include "cJSON.h"
#include <unistd.h>


#ifdef _WIN32
#include <Windows.h>
#else
#include <unistd.h>
#endif

#undef __FUNCT__
#define __FUNCT__ "main"
int main(int argc,char ** argv)
{
  PetscErrorCode 	ierr;
  char              washdir[PETSC_MAX_PATH_LEN];
  PetscBool         washdirflg = PETSC_FALSE;


  PetscInitialize(&argc,&argv,"washoptions",help);



  ierr = PetscOptionsGetString(NULL,PETSC_NULL, "-washdir",washdir,sizeof(washdir),&washdirflg);CHKERRQ(ierr);
  SAWs_Add_Document_Root(washdir);
  ierr = PetscSAWsBlock();CHKERRQ(ierr);


  cJSON *root,*fmt;
  root = cJSON_CreateObject();  
  cJSON_AddItemToObject(root, "name", cJSON_CreateString("Jack (\"Bee\") Nimble"));
  cJSON_AddItemToObject(root, "format", fmt = cJSON_CreateObject());  
  cJSON_AddStringToObject(fmt, "type", "rect");
  cJSON_AddNumberToObject(fmt, "width", 1920);
  cJSON_AddNumberToObject(fmt, "height", 1080);
  cJSON_AddFalseToObject (fmt, "interlace");
  cJSON_AddNumberToObject(fmt, "frame rate", 24);


  SAWs_WS_SendData(cJSON_PrintUnformatted(root));


  

  SAWs_WS_Exit();
  PetscFinalize();
  return 0;
}











# SAWs 

For licensing information see the comments at the top of each source and include file
  in the src/ and js/  subdirectories

See <https://bitbucket.org/saws/saws/wiki/Home> for a short introduction to SAWs.

##### Installation -- Prefered approach


```
git clone git@bitbucket.org:saws/saws.git
(or clone https://<your bitbucket login name>@bitbucket.org/saws/saws.git)
cd saws
./configure
make -j3 -C build
```
##### Testing installation

```
cp index.html build/bin/
cp -r js build/bin
cd build/bin
./SAWsInitHandler
Point your browser to localhost:8080
```
##### Installation -- Alternative approach

```
copy all the files in src/ to your application directory and add the source files to your makefile
```
#### Do Note:

To guarantee that SAWs works correctly make sure to set the root path to your SAWs's JS folder. This can be done by the following:

```
SAWs_Add_Document_Root(your_path_to_saws_js_dir);
```

*Javascript Required:*

| File        | Version           | License  |
| ------------- |:-------------:| -----:|
| jquery-1.9.1.js      | 1.9.1 | MIT |
| SAWs.js     | N/A      |   MIT |
| sigma.min.js | 1.1.0     |   MIT |
| sigma.parsers.json.min.js | 1.1.0      |    MIT |

# Websocket

## Outline
The websocket feature for SAWs started off with upgrading some of the code in the mongoose.c (C Webserver). Due to the fact mongoose C webserver has change licenses after the orginal copy of mongoose was cloned, upgrading the mongoose.c to the current version was out of the question. After some research it was discovored that one of the develpoer of mongoose forked the source code when it was MIT license on github and has continued improving it. 
## Source Code
The websocket feature was copied into the current mongoose.c source code in SAWs. Makefile requires the flag -DUSEWEBSOCKET to allow the compling of the websocket code. SAWs.c needed some edit to allow for the new structure. Two additional callbacks needed. 

### Previous Callbacks
```c
memset(&callbacks, 0, sizeof(callbacks));
callbacks.begin_request = begin_request_handler;
```

### New Callbacks

```c
memset(&callbacks, 0, sizeof(callbacks));
callbacks.begin_request = begin_request_handler;
callbacks.websocket_ready = websocket_ready_handler;
callbacks.websocket_data = websocket_data_handler;
```

```c
static void websocket_ready_handler(struct mg_connection *conn) {
  static const char *message = "connected";
  main = conn;
  mg_websocket_write(conn, WEBSOCKET_OPCODE_TEXT, message, strlen(message));
}
```

```c
static int websocket_data_handler(struct mg_connection *conn, int flags,
                                  char *data, size_t data_len) {
  (void) flags; 
  mg_websocket_write(conn, WEBSOCKET_OPCODE_TEXT, data, data_len);
  return memcmp(data, "exit", 4);
}
```

