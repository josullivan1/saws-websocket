#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "SAWs.h"

/*includes private routines because we are testing private functions*/

 /*cstruct to JSON parser*/
int JSONParser_CStruct_to_JSON(char*,int);

/*json to cStruct parser*/
int cJSON_to_CStruct_Parser(char*,char*,char*);


int main(int argc,char **argv)
{ 
  /*Make up some data*/
  char        *final_JSON,*final_JSON_Copy;
  int         length               = 3;
  char        **stringData         = malloc(sizeof(char*));
  char        **stringDataCopy     = malloc(sizeof(char*));
  char        **stringDataArray    = malloc(length * sizeof(char*));
  char        **stringDataArrayCopy= malloc(length * sizeof(char*));
  int         *intData             = malloc(sizeof(int));
  int         *intDataArray        = malloc(length * sizeof(int)); 
  int         *intDataCopy         = malloc(sizeof(int));
  int         *intDataArrayCopy    = malloc(length * sizeof(int));
  double      *doubleData          = malloc(sizeof(double));
  double      *doubleDataArray     = malloc(length * sizeof(double));
  double      *doubleDataCopy      = malloc(sizeof(double));
  double      *doubleDataArrayCopy = malloc(length * sizeof(double));
  float       *floatData           = malloc(sizeof(float));
  float       *floatDataArray      = malloc(length * sizeof(float));
  float       *floatDataCopy       = malloc(sizeof(float));
  float       *floatDataArrayCopy  = malloc(length * sizeof(float));
  char        *charData            = malloc(sizeof(char));
  char        *charDataArray       = malloc(length * sizeof(char));
  char        *charDataCopy        = malloc(sizeof(char));
  char        *charDataArrayCopy   = malloc(length * sizeof(char));
  int         *boolData            = malloc(sizeof(int)),i = 0;
  int         *boolDataArray       = malloc(length * sizeof(int));
  int         *boolDataCopy        = malloc(sizeof(int));
  int         *boolDataArrayCopy   = malloc(length * sizeof(int));
  

  *boolData       = 1;
  *boolDataCopy   = 1;
  *intData        = 41;
  *intDataCopy    = 41;
  *doubleData     = 1.234567;
  *doubleDataCopy = 1.234567;
  *floatData      = 7.65432;
  *floatDataCopy  = 7.65432;
  *charData       = 'b';
  *charDataCopy   = 'b';
  *stringData     = strdup("string");
  *stringDataCopy = "string";

  for(i=0;i<length;i++){
    boolDataArray[i]       = 1;
    boolDataArrayCopy[i]   = 1;
    intDataArray[i]        = i + 12;
    intDataArrayCopy[i]    = i + 12;
    doubleDataArray[i]     = 27.7 + i;
    doubleDataArrayCopy[i] = doubleDataArray[i];
    floatDataArray[i]      = 13.3 + i;
    floatDataArrayCopy[i]  = 13.3 + i;
    charDataArray[i]       = 'b';
    charDataArrayCopy[i]   = 'b';
    stringDataArray[i]     = strdup("string");
    stringDataArrayCopy[i] = "string";
  }


  SAWs_Initialize();
  /* Declare a directory */ 

  /* add a variable */
  SAWs_Register("/example1/residual",boolData,1,SAWs_WRITE,SAWs_BOOLEAN);
  SAWs_Register("/example1/residual2",stringData,1,SAWs_WRITE,SAWs_STRING);
  SAWs_Register("/example1/example2/example3/residual3",doubleData,1,SAWs_WRITE,SAWs_DOUBLE);
  SAWs_Register("/example4/residual4",intData,1,SAWs_WRITE,SAWs_INT);
  SAWs_Register("/example5/residual5",floatData,1,SAWs_WRITE,SAWs_FLOAT);
  SAWs_Register("/example4/example5/residual6",charData,1,SAWs_WRITE,SAWs_CHAR);
  SAWs_Register("/example4/residual7",boolDataArray,length,SAWs_WRITE,SAWs_BOOLEAN);
  SAWs_Register("/example1/residual8",stringDataArray,length,SAWs_WRITE,SAWs_STRING);
  SAWs_Register("/example1/example2/residual9",doubleDataArray,length,SAWs_WRITE,SAWs_DOUBLE);
  SAWs_Register("example1/example2/residual10",intDataArray,length,SAWs_WRITE,SAWs_INT);
  SAWs_Register("example1/example2/residual11",floatDataArray,length,SAWs_WRITE,SAWs_FLOAT);
  SAWs_Register("example1/example2/residual12",charDataArray,length,SAWs_WRITE,SAWs_CHAR);


  int json_Length = 409600;
  final_JSON = malloc(409600);
  /* parse the variable */
  JSONParser_CStruct_to_JSON(final_JSON,json_Length);
  printf("%s\n",final_JSON);
  //(*doubleData) = 2;
  final_JSON_Copy = strdup(final_JSON);
  cJSON_to_CStruct_Parser(final_JSON,"example3","residual3");
  //printf("%s\n",final_JSON);
  JSONParser_CStruct_to_JSON(final_JSON,json_Length);

  if(strcmp(final_JSON,final_JSON_Copy) != 0){
    printf("Broken JSON.\n");
  }

  cJSON_to_CStruct_Parser(final_JSON,NULL,NULL);
  JSONParser_CStruct_to_JSON(final_JSON,json_Length);
  //printf("%s\n",final_JSON);
  if(strcmp(final_JSON,final_JSON_Copy) != 0){
    printf("Broken JSON.\n");
  }

  free(final_JSON);
  free(final_JSON_Copy);
  SAWs_Finalize();

  /*free all the user data*/
  free(intData);
  free(intDataArray);
  free(intDataCopy);
  free(intDataArrayCopy);
  free(*stringData);
  free(stringData);
  free(stringDataCopy);
  free(stringDataArrayCopy);
  for(i=0;i<length;i++){
    free(stringDataArray[i]);
  }
  free(stringDataArray);
  free(doubleData);
  free(doubleDataArray);
  free(doubleDataArrayCopy);
  free(doubleDataCopy);
  free(floatData);
  free(floatDataArray);
  free(floatDataCopy);
  free(floatDataArrayCopy);
  free(charData);
  free(charDataArray);
  free(charDataCopy);
  free(charDataArrayCopy);
  free(boolData);
  free(boolDataArray);
  free(boolDataCopy);
  free(boolDataArrayCopy);

  return 0;

}
